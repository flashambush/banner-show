import { Placeholder } from 'ramda';

declare module 'ramda' {
    export function replace(pattern: string | RegExp, replacement: Placeholder, str: string): (replacement: string) => string;
    export function replace(pattern: Placeholder, replacement: string, str: string): (pattern: string) => string;
}

