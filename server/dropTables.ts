import knexCreate from 'knex';
import { BANNERS_DIR, DB_OPTIONS, TMP_FILES_DIR } from './src/constants';
import { rm } from 'fs/promises';
import { error } from '@tsigel/logger';

const knex = knexCreate({
    client: 'pg',
    connection: { ...DB_OPTIONS }
});

Promise
    .all([
        knex.schema.dropTableIfExists('users'),
        knex.schema.dropTableIfExists('banners'),
        knex.schema.dropTableIfExists('mediaPlan'),
        rm(TMP_FILES_DIR, { recursive: true }),
        rm(BANNERS_DIR, { recursive: true }),
    ])
    .catch(e => {
        error(e);
    });
