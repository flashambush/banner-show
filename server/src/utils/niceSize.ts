import { roundTo } from './utils';

export const niceSize = (bytes: number) => {
    const data = ['Kib', 'Mib', 'Gib'].reduce((acc, sizeName) => {
        if ((acc.size / 1024) > 0.5) {
            return { size: acc.size / 1024, name: sizeName };
        }
        return acc;
    }, { size: bytes, name: 'b' });

    return `${roundTo(data.size, 2)}${data.name}`;
};