import moment, { Moment } from 'moment';
import { QueryResult } from 'pg';
import { getPGClient } from '../db/connect';
import { pipe, prop } from 'ramda';
import { QueryBuilder } from 'knex';
import { knex, TableMap } from '../db/query';
import { info } from '@tsigel/logger';
import { File } from 'formidable';
import { Func } from '../../../types/types';

export type Time = Date | string | number | Moment;

export const toDate = (date: Time): Date => {
    switch (typeof date) {
        case 'object':
            if (date instanceof Date) {
                return date;
            } else {
                return date.toDate();
            }
        default:
            return new Date(date);
    }
};

export const toArray = <T>(some: T | Array<T>): Array<T> =>
    Array.isArray(some) ? some : [some];

export const dateToString = (date: Time) =>
    toDate(date).toISOString();

export const stringToMoment = pipe(toDate, moment);

export const propToMoment = (property: string) => pipe(prop<string, Date>(property), stringToMoment);
export const propToISOString = (property: string) => pipe(prop<string, Date>(property), dateToString);

export const execSql = <R>(sql: string): Promise<QueryResult<R>> => {
    const client = getPGClient();
    return client.connect()
        .then(() => client.query(`${sql};`))
        .then(result => client.end().then(() => result));
};

export const makeBdRequest = <T extends keyof TableMap>(tableName: T, builderCb: Func<[QueryBuilder<TableMap[T][0], TableMap[T][1]>], QueryBuilder<TableMap[T][0], TableMap[T][1]>>) => {
    const result = builderCb(knex(tableName));
    const sql = result.toQuery();

    info(`Db query: "${sql};"`);

    return execSql(result.toQuery());
};

export const makeTmpExtractFolderPath = (file: File) =>
    `${file.path}_tmp`;

export const getSeconds = (start: number) =>
    roundTo((Date.now() - start) / 1000, 2);

export const roundTo = (num: number, decimals: number) => {
    const part = Math.pow(10, decimals);
    return Math.round(num * part) / part;
};
