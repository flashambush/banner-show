import { Func } from '../../../types/types';


export const validate = <Data extends Record<string, any>, T extends Schema<Data>>(schema: T) =>
    (data: any): Array<ValidateError<keyof Data>> =>
        Object.entries(schema)
            .reduce<Array<ValidateError<any>>>((acc, [field, validations]: [string, Array<[Func<[any], boolean>, string]>]) => {
                validations.some(([validator, message]) => {
                    try {
                        if (!validator(data[field])) {
                            acc.push({ field, message });
                            return true;
                        }
                    } catch (e) {
                        acc.push({ field, message });
                        return true;
                    }
                });
                return acc;
            }, []);

export const getValidationDetails = (errors: Array<ValidateError<any>>): string =>
    errors.map((error) => `Wrong param ${error.field}. Error message ${error.message}`).join('\n');

type ValidateError<Key extends string | number | Symbol> = {
    field: Key,
    message: string;
}

export type Schema<T extends Record<string, any>> = {
    [Key in keyof T]: Array<[Func<[T[Key]], boolean>, string]>;
};


