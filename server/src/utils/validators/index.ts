import { is, isNil, not, pipe } from 'ramda';
import { Func } from '../../../../types/types';

export const makeValidator = <T>(template: string, validator: Func<[T], boolean>): [Func<[T], boolean>, string] =>
    [validator, template];

export const isString = makeValidator(
    'Field should be a string!',
    is(String)
);

export const required = makeValidator('Field is required!', pipe(
    isNil,
    not
));
