import { readdirSync, statSync } from 'fs';
import { join } from 'path';

export function getFilesFrom(dist: string): Array<string> {
    const files: Array<string> = [];

    const read = (localPath: string): void => {
        const result = readdirSync(localPath);
        const forRead: Array<string> = [];

        result.sort().forEach((itemName) => {
            const itemPath = join(localPath, itemName);
            if (statSync(itemPath).isDirectory()) {
                forRead.push(itemPath);
            } else {
                files.push(itemPath);
            }
        });

        forRead.forEach(read);
    };

    read(dist);

    return files;
}