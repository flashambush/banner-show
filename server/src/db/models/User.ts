import { knex } from '../connect';
import { applySpec, construct, head, map, pick, pipe, prop } from 'ramda';
import { dateToString, execSql, toDate } from '../../utils/utils';
import moment, { Moment } from 'moment';
import { QueryResult } from 'pg';


export class User {
    private static fromBd = applySpec<User.RawUser<Moment>>({
        id: prop('id'),
        name: prop('name'),
        email: prop('email'),
        role: prop('role'),
        createdAt: pipe(prop<'createdAt', string>('createdAt'), toDate, moment),
        password: prop('password')
    });
    private static toBd = applySpec<User.RawUser<string>>({
        name: prop('name'),
        email: prop('email'),
        role: prop('role'),
        createdAt: pipe<User.RawUser<Moment>, Moment, string>(prop('createdAt'), dateToString),
        password: prop('password')
    });
    private static parseUsers: (data: QueryResult) => User[] = pipe(
        prop('rows'),
        map(pipe(User.fromBd, construct(User)))
    );
    public readonly id: number = 0;
    public readonly name: string = '';
    public readonly email: string = '';
    public readonly role: string = '';
    public readonly createdAt: Moment = moment();
    public readonly password: string = '';

    constructor(raw_user: User.RawUser<Moment>) {
        Object.assign(this, raw_user);
    }

    public static getByUniqueProperty(property: 'id' | 'email' | 'name', value: string | number): Promise<User | undefined> {
        const querySQL = knex('users')
            .select('*')
            .where(property, value)
            .toQuery();

        return execSql(querySQL)
            .then(User.parseUsers)
            .then<User | undefined>(head);
    }

    public static getLastUser(): Promise<User | undefined> {
        const querySQL = knex('users')
            .select('*')
            .orderBy('id', 'desc')
            .limit(1)
            .toQuery();

        return execSql(querySQL)
            .then(User.parseUsers)
            .then<User | undefined>(head);
    }

    public static create(user: User.CreateUserData): Promise<User> {
        const createdAt = moment().utc();

        const querySQL = knex('users')
            .insert(User.toBd({
                ...user,
                createdAt
            }))
            .onConflict('email')
            .ignore()
            .returning('*')
            .toQuery();

        return execSql(querySQL)
            .then(User.parseUsers)
            .then<User>(head);
    }

    public getClientData(): Pick<User, 'email' | 'name'> {
        return pick(['email', 'name'], { ...this }) as Pick<User, 'email' | 'name'>;
    }
}

export namespace User {
    type CreateUserFields = 'name' | 'password' | 'email' | 'role';

    export type CreateUserData = Pick<RawUser<Moment>, CreateUserFields>;

    export type RawUser<Date> = {
        id: number;
        name: string;
        email: string;
        role: string;
        createdAt: Date;
        password: string;
    }
}
