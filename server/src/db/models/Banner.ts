import { applySpec, construct, flatten, head, identity, map, pipe, prop } from 'ramda';
import {
    dateToString,
    execSql,
    makeBdRequest,
    makeTmpExtractFolderPath,
    propToMoment,
    toArray
} from '../../utils/utils';
import { knex } from '../connect';
import { QueryResult } from 'pg';
import { BannerInfo, BannerStatus, BannerType, Without } from '../../../../types/types';
import moment, { Moment } from 'moment';
import { base58Encode, blake2b, stringToBytes } from '@waves/ts-lib-crypto';
import Router from 'koa-router';
import { info, warn } from '@tsigel/logger';
import { BadRequest, InternalError, NotFound, PermissionDenied } from '../../server/errors';
import { AuthState } from '../../server/middlewares/auth/types';
import { limit, select, update, where } from '../query';
import { Queue } from '../../services/Queue';
import { File } from 'formidable';
import { Process } from '../../services/Process';
import { makeProcessId } from '../../server/middlewares/mediaPlan/utils';
import { processBanner } from '../../processBanner/processBanner';
import { MediaPlan } from './MediaPlan';
import { ProcessPack } from '../../services/ProcessPack';
import { userRequestManager } from '../../server/UserRequestManager';
import { PrepareProcessingData } from '../../processBanner/helpers';

export class Banner implements BannerInfo<Moment> {
    private static fromBd = applySpec<BannerInfo<Moment>>({
        banner_id: prop('banner_id'),
        mediaPlanId: prop('mediaPlanId'),
        width: prop('width'),
        height: prop('height'),
        type: prop('type'),
        platform: prop('platform'),
        bannerStatus: prop('bannerStatus'),
        version: prop('version'),
        details: prop('details'),
        createdAt: propToMoment('createdAt'),
    });
    public static parse: (data: QueryResult) => Banner[] = pipe(
        prop('rows'),
        map(pipe(Banner.fromBd, construct(Banner)))
    );
    private static makeId = pipe(
        stringToBytes,
        blake2b,
        base58Encode
    );
    private static toBd = applySpec<BannerInfo<Moment>>({
        banner_id: pipe(Banner.makeStringId, Banner.makeId),
        mediaPlanId: prop('mediaPlanId'),
        width: prop('width'),
        height: prop('height'),
        type: prop('type'),
        platform: prop('platform'),
        bannerStatus: prop('bannerStatus'),
        version: prop('version'),
        details: prop('details'),
        createdAt: () => dateToString(moment().toDate())
    });
    public readonly banner_id: string = '';
    public readonly mediaPlanId: number = 0;
    public readonly width: number = 0;
    public readonly height: number = 0;
    public readonly type: BannerType = BannerType.Html;
    public readonly platform: string = '';
    public readonly bannerStatus: BannerStatus = BannerStatus.Uploaded;
    public readonly version: number = 0;
    public readonly details: string = '';
    public readonly createdAt: Moment = moment();

    constructor(banner: BannerInfo<Moment>) {
        Object.assign(this, banner);
    }

    public static create(data: Without<BannerInfo<Moment>, 'banner_id' | 'createdAt'>[]): Promise<Array<Banner>> {
        const querySQL = knex('banners')
            .insert(data.map(Banner.toBd))
            .returning('*')
            .toQuery();

        return execSql(querySQL)
            .then(Banner.parse);
    }

    public static getByMediaPlanId(mediaPlanId: number, query: Pick<BannerInfo<string>, 'platform' | 'bannerStatus'>): Promise<Array<Banner>> {
        return makeBdRequest(
            'banners',
            pipe(
                select('*'),
                where('mediaPlanId', mediaPlanId),
                where('bannerStatus', BannerStatus.Deleted, '!='),
                query?.platform != null ? where('platform', query.platform) : identity,
                query?.bannerStatus != null ? where('bannerStatus', query.bannerStatus) : identity,
                limit(201),
            )
        )
            .then(Banner.parse);
    }

    public static getById(banner_id: string): Promise<Banner | undefined> {
        return makeBdRequest(
            'banners',
            pipe(
                select('*'),
                where('banner_id', banner_id)
            )
        )
            .then(Banner.parse)
            .then<Banner | undefined>(head);
    }

    public static update(banner: Partial<BannerInfo<string>> & { banner_id: string }): Promise<Banner> {
        return makeBdRequest('banners',
            pipe(
                where<BannerInfo<string>, 'banner_id'>('banner_id', banner.banner_id),
                update(banner)
            )
        )
            .then(Banner.parse)
            .then<Banner>(head);
    }

    public static getByMediaPlanIdM(options: Options = Object.create(null)): Router.IMiddleware {
        return (ctx, next) =>
            Banner.getByMediaPlanId(ctx.params.id, { ...ctx.query, ...(options.approvedOnly ? { bannerStatus: BannerStatus.Approved } : {}) })
                .then((banners) => {
                    info('Get banners by mediaPlan id.');

                    ctx.body = {
                        type: 'banner-list',
                        content: banners.slice(0, 200),
                        hasNextPage: banners.length > 200
                    };

                    return next();
                });
    }

    public static getByIdM(options: Options = Object.create(null)): Router.IMiddleware<AuthState> {
        return (ctx, next) =>
            Banner.getById(ctx.params.banner_id)
                .then(banner => {
                    info(`Get banner by id ${ctx.params.banner_id}.`);

                    if (!banner) {
                        ctx.status = 404;
                    } else if (!ctx.state?.s_user?.id) {
                        if (options.approvedOnly && banner.bannerStatus !== BannerStatus.Approved) {
                            return Promise.reject(new PermissionDenied());
                        }
                    }

                    ctx.body = {
                        type: 'banner',
                        content: banner
                    };
                    return next();
                });
    }

    public static approveM(): Router.IMiddleware {
        return (ctx, next) =>
            Banner.getById(ctx.params.banner_id)
                .then((banner) => {
                    if (!banner) {
                        return Promise.reject(new NotFound());
                    }

                    if (banner.bannerStatus !== BannerStatus.Uploaded) {
                        return Promise.reject(new BadRequest('Can\'t approve this banner!'));
                    }

                    return Banner.update({
                        ...banner,
                        createdAt: dateToString(banner.createdAt),
                        bannerStatus: BannerStatus.Approved
                    });
                })
                .then((updated) => {
                    ctx.body = {
                        type: 'updated-banner',
                        content: updated
                    };
                    return next();
                });
    }

    public static updateM(queue: Queue<any>): Router.IMiddleware {
        return (ctx, next) =>
            Banner.getById(ctx.params.banner_id)
                .then((banner) => {
                    if (!banner) {
                        return Promise.reject(new NotFound());
                    }

                    if (banner.bannerStatus === BannerStatus.Approved) {
                        return Promise.reject(new BadRequest('Permission denied! Banner is approved!'));
                    }

                    const files = flatten(
                        Object.values<File | Array<File>>((ctx.request.files ?? {}) as any)
                            .map((item: File | Array<File>) => toArray(item))
                    );

                    if (files.length > 1) {
                        return Promise.reject(new BadRequest('Uploaded more then 1 file!'));
                    }

                    if (files.length === 0) {
                        return Promise.reject(new BadRequest('Has no uploaded files!'));
                    }

                    const [file] = files;

                    return MediaPlan.getById(banner.mediaPlanId)
                        .then((mediaPlan) => {
                            const id = `update-${banner.banner_id}`;
                            if (!mediaPlan) {
                                return Promise.reject(new InternalError('Has no mediaPlan with id from banner!'));
                            }

                            const process = new Process(
                                makeProcessId(id, file, banner.platform),
                                () => processBanner(
                                    file,
                                    makeTmpExtractFolderPath(file),
                                    banner.platform,
                                    mediaPlan.client,
                                    mediaPlan.campaign
                                )
                            );
                            const pack = new ProcessPack<PrepareProcessingData>(queue, id, [process]);

                            userRequestManager.addJob({
                                author: ctx.state.s_user,
                                id,
                                processPack: pack
                            });

                            pack.once('finish', ([processed]) => {
                                if (!processed.success) {
                                    warn('Fail update banner.', processed.data);
                                    return void 0;
                                }

                                Banner
                                    .update({
                                        banner_id: banner.banner_id,
                                        bannerStatus: processed.data.bannerStatus,
                                        version: banner.version + 1,
                                        details: processed.data.details,
                                        platform: processed.data.platform,
                                        height: processed.data.bannerSize.height,
                                        width: processed.data.bannerSize.width,
                                        type: BannerType.Html
                                    })
                                    .then((data) => info(`Banner updated successfully!`, data))
                                    .catch((warn));
                            });

                            ctx.body = {
                                type: 'process-run',
                                id: pack.id
                            };

                            return next();
                        });
                });
    }

    public static deleteM(): Router.IMiddleware {
        return (ctx, next) =>
            Banner.getById(ctx.params.banner_id)
                .then(banner => {
                    if (!banner) {
                        return Promise.reject(new NotFound());
                    }

                    if (banner.bannerStatus === BannerStatus.Deleted) {
                        return Promise.reject(new BadRequest('Banner already removed!'));
                    }

                    return Banner.update({
                        ...banner,
                        createdAt: dateToString(banner.createdAt),
                        bannerStatus: BannerStatus.Deleted
                    });
                })
                .then((updated) => {
                    ctx.body = {
                        type: 'updated-banner',
                        content: updated
                    };
                    return next();
                });
    }


    public static makeBannerId(data: Pick<BannerInfo<Moment>, 'width' | 'height' | 'type' | 'platform' | 'mediaPlanId'>): string {
        return Banner.makeId(Banner.makeStringId(data));
    }

    private static makeStringId(data: Pick<BannerInfo<Moment>, 'width' | 'height' | 'type' | 'platform' | 'mediaPlanId'>): string {
        return [
            data.width,
            data.height,
            data.type,
            data.platform,
            data.mediaPlanId
        ].join('-');
    }
}

export type Options = {
    approvedOnly?: boolean
}
