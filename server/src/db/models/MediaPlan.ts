import moment, { Moment } from 'moment';
import { applySpec, construct, head, identity, map, omit, pipe, prop, toPairs } from 'ramda';
import { execSql, makeBdRequest, propToISOString, propToMoment } from '../../utils/utils';
import { QueryResult } from 'pg';
import Router from 'koa-router';
import { knex, limit, select, where } from '../query';
import { Banner } from './Banner';
import { info } from '@tsigel/logger';
import { AuthState } from '../../server/middlewares/auth/types';
import { QueryBuilder } from 'knex';
import { BannerInfo, Func, MediaPlanInfo, Without } from '../../../../types/types';


export class MediaPlan implements MediaPlanInfo<Moment> {
    private static parseMediaPlanSchema = {
        id: prop('id'),
        author: prop('author'),
        client: prop('client'),
        brand: prop('brand'),
        campaign: prop('campaign'),
        createdAt: propToMoment('createdAt'),
    };
    private static serializeMediaPlanSchema = {
        author: prop('author'),
        client: prop('client'),
        brand: prop('brand'),
        campaign: prop('campaign'),
        createdAt: propToISOString('createdAt'),
    };
    private static fromBd = applySpec<MediaPlanInfo<Moment>>(MediaPlan.parseMediaPlanSchema);
    private static toBd = applySpec<Without<MediaPlanInfo<string>, 'id'>>(MediaPlan.serializeMediaPlanSchema);
    private static parse: (data: QueryResult) => MediaPlan[] = pipe(
        prop('rows'),
        map(pipe(MediaPlan.fromBd, construct(MediaPlan)))
    );
    public readonly id: number = 0;
    public readonly author: number = 0;
    public readonly client: string = '';
    public readonly brand: string = '';
    public readonly campaign: string = '';
    public readonly createdAt: Moment = moment();
    public readonly banners: Array<Banner> = [];

    constructor(banner: MediaPlanInfo<Moment>) {
        Object.assign(this, banner);
    }

    public static create(data: Without<MediaPlanInfo<Moment>, 'id' | 'createdAt'> & { banners: Array<Without<BannerInfo<Moment>, 'banner_id' | 'mediaPlanId' | 'createdAt'>> }): Promise<MediaPlanInfo<Moment> & { banners: Array<Banner> }> {
        const querySQL = knex('mediaPlan')
            .insert(MediaPlan.toBd({
                ...omit(['banners'], data),
                createdAt: moment()
            }))
            .returning('*')
            .toQuery();

        return execSql(querySQL)
            .then(MediaPlan.parse)
            .then<MediaPlan>(head)
            .then((mediaPlan) =>
                Banner.create(data.banners.map(item => ({ ...item, mediaPlanId: mediaPlan.id })))
                    .then(banners => (({ ...mediaPlan, banners }))));
    }

    public static getList(query: Pick<MediaPlanInfo<string>, 'brand' | 'client'>): Promise<Array<MediaPlan>> {
        const filters = toPairs(query)
            .map(([key, value]) => where(key, value));

        const callFilters = <T extends QueryBuilder<any, any>>(modifiers: Array<Func<[T], T>>): (builder: T) => T =>
            (modifiers.length ? pipe.apply(null, modifiers as any) : identity) as any

        return makeBdRequest(
            'mediaPlan',
            pipe(
                select('*'),
                callFilters(filters),
                limit(201),
            )
        )
            .then(MediaPlan.parse);
    }

    public static getById(id: number): Promise<MediaPlan | undefined> {
        return makeBdRequest(
            'mediaPlan',
            pipe(
                select('*'),
                where('id', id)
            )
        )
            .then(MediaPlan.parse)
            .then<MediaPlan | undefined>(head);
    }

    public static getListM(): Router.IMiddleware<AuthState> {
        return (ctx, next) => MediaPlan.getList(ctx.query)
            .then((mediaPlans) => {
                info('Get mediaPlans list.');
                ctx.body = {
                    type: 'mediaPlan-list',
                    content: mediaPlans.slice(0, 100),
                    hasNextPage: mediaPlans.length > 100
                };
                return next();
            });
    }

    public static getByIdM(): Router.IMiddleware {
        return (ctx, next) =>
            MediaPlan.getById(ctx.params.id)
                .then((mediaPlan) => {
                    info(`Get mediaPlan by id ${ctx.params.id}.`);

                    if (!mediaPlan) {
                        ctx.status = 404;
                    }

                    ctx.body = {
                        type: 'mediaPlan',
                        content: mediaPlan
                    };
                    return next();
                });
    }
}
