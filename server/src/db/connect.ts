import knexCreate from 'knex';
import { DB_OPTIONS } from '../constants';
import { Client } from 'pg';

export const knex = knexCreate({
    client: 'pg',
    connection: { ...DB_OPTIONS }
});

export const getPGClient = () => new Client(DB_OPTIONS);
