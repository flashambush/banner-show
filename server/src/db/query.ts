import { QueryBuilder } from 'knex';
import { knex as _knex } from './connect';
import { User } from './models/User';
import { BannerInfo, MediaPlanInfo, Without } from '../../../types/types';

export const where = <T extends Record<string, any>, Key extends keyof T>(key: Key, value: T[Key] | Array<T[Key]>, operator?: string) =>
    (builder: QueryBuilder<T>): QueryBuilder<T> =>
        operator
            ? builder.where(key as string, operator, value)
            : builder.where(key, value as any)

export const limit =  <T extends QueryBuilder<any, any>>(count: number) =>
    (builder: T) =>
        builder.limit(count);

export const update =  <I extends Record<any, any>, T extends QueryBuilder<I>>(data: Partial<I>) =>
    (builder: T): T =>
        builder.update(data as any, Object.keys(data)) as any;

export const select = <T extends QueryBuilder<any, any>>(selectValue: string) =>
    (builder: T) =>
        builder.select(selectValue);

export const knex = <Key extends keyof TableMap>(tableName: Key): QueryBuilder<TableMap[Key][0], TableMap[Key][1]> =>
    _knex(tableName);


export type TableMap = {
    'users': [Without<User.RawUser<string>, 'id'>, User.RawUser<string>];
    'mediaPlan': [MediaPlanInfo<string>, MediaPlanInfo<string>];
    'banners': [BannerInfo<string>, BannerInfo<string>];
}
