import { Process } from './Process';
import { not, pipe, propEq } from 'ramda';
import { info } from '@tsigel/logger';

export class Queue<T> {
    private readonly treads: number;
    private waiting: Array<Process<T>> = [];
    private inProgress: Array<Process<T>> = [];

    constructor(treads: number) {
        this.treads = treads;
    }

    public add(process: Process<T>): void {
        this.waiting.push(process);
        this.checkRun();
    }

    private checkRun(): void {
        if (this.inProgress.length < this.treads) {
            if (!this.waiting.length) {
                info('Queue is empty.');
                return void 0;
            }

            info('Queue run next process.');
            const process = this.waiting.shift() as Process<T>;
            this.inProgress.push(process);

            process.run()
                .then(this.onDone.bind(this));

            return this.checkRun();
        }
    }

    private onDone(): void {
        this.inProgress = this.inProgress.filter(pipe(propEq('status', 'done'), not));
        this.checkRun();
    }
}
