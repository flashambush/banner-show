import { EventEmitter } from 'typed-ts-events';
import { getSeconds } from '../utils/utils';
import { error, info } from '@tsigel/logger';
import { Func } from '../../../types/types';


export class Process<T> extends EventEmitter<Events<T>> {
    public readonly id: string;
    public status: 'waiting' | 'pending' | 'done' = 'waiting';
    public data: T | Error | void = void 0;
    private readonly callback: ProcessCallback<T>;

    constructor(id: string, callback: ProcessCallback<T>) {
        super();
        this.id = id;
        this.callback = callback;
    }

    public run(): Promise<Omit<ProcessEndEvent<T>, 'id'>> {
        info(`Run process with id ${this.id}`);
        const start = Date.now();
        this.trigger('start', { id: this.id });
        this.status = 'pending';

        return this.callback(this)
            .then(data => {
                info(`Process with id ${this.id} ended success after ${getSeconds(start)}s`);
                return { success: true, data, time: getSeconds(start) } ;
            })
            .catch((e: Error) => {
                error(`Process with id ${this.id} ended with error ${e.message} after ${getSeconds(start)}s`);
                error(e.stack);
                return { success: false, data: e, time: getSeconds(start) };
            })
            .then(data => {
                this.data = data.data;
                this.terminate(data);
                return data;
            });
    }

    private terminate(data: Omit<ProcessEndEvent<T>, 'id'>): void {
        this.status = 'done';
        this.trigger('end', {
            id: this.id,
            ...data
        } as ProcessEndEvent<T>);
        this.off();
    }
}

export type ProcessCallback<T> = Func<[Process<T>], Promise<T>>

export type Events<T> = {
    'log': string;
    'start': { id: string };
    'end': ProcessEndEvent<T>
}

export type ProcessEndEvent<T> = {
    id: string;
    success: true;
    time: number;
    data: T
} | {
    id: string;
    success: false;
    time: number;
    data: Error;
}
