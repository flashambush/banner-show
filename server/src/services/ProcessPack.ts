import { Process, ProcessEndEvent } from './Process';
import { Queue } from './Queue';
import { EventEmitter } from 'typed-ts-events';
import { getSeconds } from '../utils/utils';
import { info } from '@tsigel/logger';


export class ProcessPack<T> extends EventEmitter<Events<T>> {
    public readonly id: string;
    private readonly processes: Array<Process<T>>;
    private startTime: number = 0;
    private resultList: Array<ProcessEndEvent<T>> = [];

    constructor(queue: Queue<T>, id: string, processes: Array<Process<T>>) {
        super();
        this.id = id;
        this.processes = processes;

        processes[0].on('start', this.onStartFirstProcess, this);

        processes.forEach((process) => {
            process.on('end', this.onProcessEnd, this);
            queue.add(process);
        });
    }

    private onProcessEnd(data: ProcessEndEvent<T>): void {
        this.resultList.push(data);
        this.makeProgressEvent();

        if (this.resultList.length === this.processes.length) {
            info(`Pack with id ${this.id} finished after ${getSeconds(this.startTime)}s`);
            this.trigger('finish', this.resultList);

            this.processes.forEach(process => process.off());
            this.off();
        }
    }

    private makeProgressEvent(): void {
        const progress = this.resultList.length / this.processes.length;
        this.trigger('progress', {
            progress,
            time: getSeconds(this.startTime),
            bannerIndexInProgress: this.resultList.length,
            bannersCount: this.processes.length
        });
    }

    private onStartFirstProcess() {
        this.startTime = Date.now();
    }
}

export namespace ProcessPack {
    export type ProgressEvent = {
        progress: number;
        bannerIndexInProgress: number;
        bannersCount: number;
        time: number;
    }
}

export type Events<T> = {
    'progress': ProcessPack.ProgressEvent;
    'finish': Array<ProcessEndEvent<T>>;
}
