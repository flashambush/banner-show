import { loadGoogleSheets, Row } from 'parse-googlesheets';
import { GOOGLE_API_KEY, TMP_FILES_DIR, TT_RANGE, TT_SCHEMA, TT_SHEET_ID } from '../constants';
import { promises } from 'fs';
import { join } from 'path';
import { bind } from 'ramda';

export const loadMakeBannerSchemas = () =>
    loadGoogleSheets(TT_SCHEMA, TT_RANGE, TT_SHEET_ID, GOOGLE_API_KEY)
        .then(data =>  promises.writeFile(join(TMP_FILES_DIR, `loadGoogleSheets.json`), JSON.stringify(data)));

export const getGoogleSheets = (): Promise<Row<typeof TT_SCHEMA>[]> =>
    promises
        .readFile(join(TMP_FILES_DIR, `loadGoogleSheets.json`), 'utf8')
        .then(bind(JSON.parse, JSON));
