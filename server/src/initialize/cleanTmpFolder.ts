import { promises } from 'fs';
import { TMP_FILES_DIR } from '../constants';
import { join } from 'path';
import { always } from 'ramda';

const mkDir = (path: string) =>
    () =>
        promises.mkdir(path)
            .then(always(void 0));

export const cleanTmpFolder = () =>
    promises.access(TMP_FILES_DIR)
        .then(() => promises.readdir(TMP_FILES_DIR))
        .then((names) => {
            return Promise.all(names.map((name) => {
                return promises.stat(join(TMP_FILES_DIR, name)).then(stat => stat.isDirectory()
                    ? promises.rm(join(TMP_FILES_DIR, name), { recursive: true })
                    : promises.unlink(join(TMP_FILES_DIR, name))
                );
            }));
        })
        .then(always(void 0))
        .catch(mkDir(TMP_FILES_DIR));
