import { promises } from 'fs';
import { BANNERS_DIR } from '../constants';

export const checkFilesFolder = () =>
    promises.access(BANNERS_DIR)
        .catch(() => promises.mkdir(BANNERS_DIR));