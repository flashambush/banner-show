import { error } from '@tsigel/logger';
import { loadGoogleSheets, required, string } from 'parse-googlesheets';
import { bind, map, pipe } from 'ramda';
import { User } from '../db/models/User';

const schema = {
    email: { parse: required(string), columnName: 'email' },
    name: { parse: required(string), columnName: 'name' },
    password: { parse: required(string), columnName: 'password' },
    role: { parse: required(string), columnName: 'role' },
};


export const initializeUsers = () =>
    User.getLastUser()
        .then(user => {
            if (user) {
                return void 0;
            }
            return loadGoogleSheets(schema, encodeURIComponent('Лист1'), '1KJ_T27szit9qA0NxqgudSWdfA7xIlCKl32FpbrrnL2g', 'AIzaSyA02DRcdeNGrWA6ASsHexwG_W6i8CCPie8') // TODO
                .then(pipe(
                    map(User.create),
                    bind(Promise.all, Promise)
                ))
                .catch(e => {
                    error(e);
                    return Promise.reject(e);
                });
        });
