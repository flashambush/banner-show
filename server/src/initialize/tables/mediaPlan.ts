import { checkTable } from './checkTable';

export const checkMediaPlanTable = () =>
    checkTable('mediaPlan', (tableBuilder) => {
        tableBuilder.increments('id').primary().comment('Auto-generated id');
        tableBuilder.integer('author').notNullable();
        tableBuilder.string('client').notNullable();
        tableBuilder.string('brand').notNullable();
        tableBuilder.string('campaign').notNullable();
        tableBuilder.timestamp('createdAt', { useTz: true }).notNullable();
    });
