import { checkTable } from './checkTable';

export const checkUsersTable = () =>
    checkTable('users', (tableBuilder) => {
        tableBuilder.increments('id').primary().comment('Auto-generated id');
        tableBuilder.string('name').unique().notNullable();
        tableBuilder.string('email').unique().notNullable();
        tableBuilder.string('role').notNullable();
        tableBuilder.timestamp('createdAt', { useTz: true });
        tableBuilder.string('password').notNullable();
    });
