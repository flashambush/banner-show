import { CreateTableBuilder } from 'knex';
import { knex } from '../../db/connect';
import { equals, ifElse, pipe } from 'ramda';
import { info, error } from '@tsigel/logger';

const logOnCall = (message: string) => <T>(data: T): T => {
    info(message)
    return data;
}

export const checkTable = (db_name: string, make_table_cb: Callback) =>
    knex
        .schema
        .hasTable(db_name)
        .then(ifElse(
            equals(true),
            logOnCall(`Table "${db_name}" is exists! Has table is`),
            pipe(
                logOnCall(`Create "${db_name}" table! Has table is`),
                () => knex.schema.createTable(db_name, make_table_cb)
            )
        ))
        .catch((e: Error) => {
            error(`Can't create table "${db_name}", ${e}`);
            return Promise.reject(e);
        });

type Callback = (tableBuilder: CreateTableBuilder) => void;
