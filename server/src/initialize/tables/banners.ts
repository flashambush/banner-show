import { checkTable } from './checkTable';

export const checkBannersTable = () =>
    checkTable('banners', (tableBuilder) => {
        tableBuilder.string('banner_id').notNullable().unique();
        tableBuilder.integer('mediaPlanId').notNullable();
        tableBuilder.integer('width').notNullable();
        tableBuilder.integer('height').notNullable();
        tableBuilder.string('type').notNullable();
        tableBuilder.string('platform').notNullable();
        tableBuilder.string('bannerStatus').notNullable();
        tableBuilder.string('details');
        tableBuilder.integer('version').notNullable();
        tableBuilder.timestamp('createdAt', { useTz: true }).notNullable();
    });
