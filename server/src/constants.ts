import { join } from 'path';
import { randomSeed } from '@waves/ts-lib-crypto';
import { number, required, string, boolean } from 'parse-googlesheets';
import { EventEmitter } from 'typed-ts-events';

export const ROOT_DIR = join(__dirname, '..');
export const TMP_FILES_DIR = join(ROOT_DIR, 'uploads');
export const BANNERS_DIR = join(ROOT_DIR, 'files');
export const DB_OPTIONS = {
    user: 'postgres',
    host: '0.0.0.0',
    database: 'postgres',
    password: 'eukanuba',
    port: 5432
};

export const SECRET = process.env.SECRET ?? 'SECRET SEED' ?? randomSeed(20);
export const BANNER_PREPARE_TREADS = Number(process.env.BANNER_PREPARE_TREADS) || 2;

export const messenger = new EventEmitter();

export const GOOGLE_API_KEY = 'AIzaSyBcPECwiIN4TJH-l7ktcnJGHqd4fiVF7Aw';
export const TT_SHEET_ID = '1aJ3Yr7g_l0QlsVY6-Xe5fMUHExMJmdTQlVdyo2tdBLk';
export const TT_RANGE = 'tt';
export const TT_SCHEMA = {
    title: {
        parse: required(string),
        columnName: 'Площадка'
    },
    libTag: {
        parse: string,
        columnName: 'библиотека площадки'
    },
    createjsLibTag: {
        parse: string,
        columnName: 'библиотека createjs'
    },
    metaTags: {
        parse: string,
        columnName: 'мета'
    },
    exitScript: {
        parse: string,
        columnName: 'экзит по клику'
    },
    exitTag: {
        parse: string,
        columnName: 'экзит ссылка'
    },
    bodyScript: {
        parse: string,
        columnName: 'экзит ссылка'
    },
    initScript: {
        parse: string,
        columnName: 'экзит ссылка'
    },
    stageReadyScript: {
        parse: string,
        columnName: 'экзит ссылка'
    },
    maxSize: {
        parse: number,
        columnName: 'макимальный вес'
    },
    inArchive: {
        parse: boolean,
        columnName: 'вес архива'
    }
};
