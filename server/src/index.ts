import 'dotenv/config';
import Koa from 'koa';
import { cleanTmpFolder } from './initialize/cleanTmpFolder';
import { checkUsersTable } from './initialize/tables/users';
import { checkBannersTable } from './initialize/tables/banners';
import { launchServer } from './server';
import { initializeUsers } from './initialize/initializeUsers';
import { checkMediaPlanTable } from './initialize/tables/mediaPlan';
import { checkFilesFolder } from './initialize/checkFilesFolder';
import { loadMakeBannerSchemas } from './services/loadMakeBannerSchemas';
import { configure, error } from '@tsigel/logger';

configure('timeZone', 3);

Promise
    .all([
        checkUsersTable(),
        checkBannersTable(),
        checkMediaPlanTable(),
        cleanTmpFolder()
            .then(loadMakeBannerSchemas),
        checkFilesFolder(),
    ])
    .then(initializeUsers)
    .then(() => {
        launchServer(new Koa());
    })
    .catch((e) => {
        error(e);
        process.exit(1);
    });
