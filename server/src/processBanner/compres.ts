import { createWriteStream } from 'fs';
import { relative } from 'path';
import archiver from 'archiver';
import { info } from '@tsigel/logger';

export const compress = (files: Array<string>, folder: string, out: string, level?: number): Promise<number> => {
    info(`Make archive with name: ${out}`);

    const output = createWriteStream(out);
    const archive = archiver('zip', {
        zlib: { level: level ?? 6 }
    });

    archive.pipe(output);

    files.forEach(path => {
        archive.file(path, { name: relative(folder, path) });
    });

    return archive.finalize()
        .then(() => archive.pointer());
};
