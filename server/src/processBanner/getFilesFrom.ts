import { promises } from 'fs';
import { join } from 'path';
import { flatten, map } from 'ramda';

export const getFilesFrom = (path: string): Promise<Array<string>> =>
    promises.stat(path)
        .then((stat) => stat.isDirectory()
            ? promises.readdir(path)
                .then(map(name => join(path, name)))
                .then((list => Promise.all(list.map(getFilesFrom))))
                .then(flatten)
            : Promise.resolve([path])
        );