import { minify } from 'uglify-js';
import { getFilesFrom } from './getFilesFrom';
import { filter, map, propEq } from 'ramda';
import { join, parse } from 'path';
import { promises } from 'fs';
import { asyncMap } from '@tsigel/async-map';

export const uglifyJsDeep = (path: string, treads?: number): Promise<Array<string>> =>
    getFilesFrom(path)
        .then(map(parse))
        .then(filter(propEq('ext', '.js')))
        .then((files) => asyncMap(treads ?? 3, (data) =>
            promises.readFile(join(data.dir, data.base), 'utf8')
                .then(code => minify(code))
                .then(output => promises.writeFile(join(data.dir, data.base), output.code))
                .then(() => join(data.dir, data.base)), files)
        );
