import { promises } from 'fs';
import { roundTo } from '../utils/utils';
import extractZip from 'extract-zip';
import imagemin from 'imagemin';
import imageminJpegtran from 'imagemin-jpegtran';
import imageminPngquant from 'imagemin-pngquant';
import { uglifyJsDeep } from './uglifyJsDeep';
import { getFilesFrom } from './getFilesFrom';
import {
    __, add,
    always,
    curry,
    find,
    flatten,
    groupBy,
    map,
    mergeAll,
    not,
    pick,
    pipe,
    prop,
    propEq,
} from 'ramda';
import { format, join, parse, ParsedPath, relative } from 'path';
import { minify } from 'html-minifier';
import { Size } from '../server/middlewares/mediaPlan/types';
import { File } from 'formidable';
import { info } from '@tsigel/logger';
import { BannerStatus, Func } from '../../../types/types';
import { imageSize } from 'image-size';
import { compress } from './compres';

const TARGET_ARCHIVE_FILE_NAME = 'banner.zip';

export const log = <T>(message: string, callback: Func<[], Promise<T>>): Promise<T> => {
    const start = Date.now();
    return callback()
        .then((data) => {
            info(message.replace('{time}', String(roundTo((Date.now() - start) / 1000, 2) + 's')));
            return data;
        });
};

export const makeOutDirectory = (directory: string) =>
    promises.mkdir(directory);

export const extract = (path: string, out: string): Promise<void> =>
    log('Extract zip: {time}', () => extractZip(path, { dir: out }));

export const compressImages = (rootDir: string, quality: [number, number] = [0.6, 0.8]) =>
    log('Image min: {time}', () => imagemin([`${rootDir}/*.{jpg,png}`], {
        destination: rootDir,
        plugins: [
            imageminJpegtran(),
            imageminPngquant({
                quality
            })
        ]
    }));

export const getBackupImageFile = (files: Record<string, ParsedPath[]>): ParsedPath | null => {
    const availableExtensions = ['.jpg', '.gif', '.png'];
    const backupNameParts = ['zagl', 'backup'];
    let result: ParsedPath | null = null;

    availableExtensions.some((ext) => {
        if (!files[ext]) {
            return false;
        }
        return files[ext].some((path) => {
            if (backupNameParts.some(part => path.name.includes(part))) {
                result = path;
                return true;
            }
            return false;
        });
    });

    return result;
};

export const getImageSize = (path: string): Promise<Size> =>
    log('Current image size: {time}', () => new Promise((resolve, reject) => {
        imageSize(path, (err, result) => {
            if (err) {
                reject(err);
                return void 0;
            }

            if (!result?.width || !result.height) {
                throw new Error('Can\'t read image size!');
            }

            resolve(pick(['width', 'height'], result) as Size);
        });
    }));

export const compressJs = (rootDir: string) =>
    log('Uglify: {time}', () => uglifyJsDeep(rootDir));

export const getFilesHashByFolder = (rootDir: string): Promise<Record<string, ParsedPath[]>> =>
    log('Read extracted folder: {time}', () => getFilesFrom(rootDir))
        .then(pipe(
            map(parse),
            groupBy(prop('ext'))
        ));

export const validateHTMLCount = (data: Record<string, ParsedPath[]>): ParsedPath => {
    if (!data['.html']) {
        throw new Error('Has no index html file in archive!');
    }
    if (data['.html'].length > 1) {
        throw new Error('HTML files length more than 1');
    }
    return data['.html'][0];
};

export const getHTMLCode = (html: ParsedPath) =>
    promises
        .readFile(join(html.dir, html.base), 'utf8');

export const replaceHtml = curry((targetFileName: string, root: string, html: ParsedPath, code: string) =>
    log('Remove old html and create new. {time}', () =>
        Promise
            .all([
                promises.writeFile(join(root, targetFileName), code),
                promises.unlink(join(html.dir, html.base))
            ]))
        .then(always(code))
);

export const findPlatform = (platformTitle: string) => (googleSheets: Array<Platform>): Platform => {
    const platform = find<Platform>(propEq('title', platformTitle), googleSheets);

    if (!platform) {
        throw new Error(`Has no platform with title ${platformTitle}!`);
    }
    return platform;
};

export const getFilesSize = (root: string, files: ParsedPath[]): Promise<Record<string, number>> =>
    log('Calculate size of files of files hash map: {time}', () =>
        Promise
            .all(files.map(file => {
                const localPath = relative(root, join(file.dir, file.base));
                return promises.stat(join(file.dir, file.base))
                    .then(({ size }) => ({ [localPath]: size }));
            }))
            .then(mergeAll)
    );

export const addBannerStatus = (data: Omit<PrepareProcessingData, 'bannerStatus'>, platform: Platform): PrepareProcessingData => {
    if (platform.maxSize == null) {
        return {
            ...data,
            bannerStatus: BannerStatus.Uploaded,
        };
    }

    const maxSize = platform.maxSize * 1024;
    if (platform.inArchive) {
        return data.archiveSize > maxSize
            ? { ...data, bannerStatus: BannerStatus.Error, details: 'Archive size is too big!' }
            : { ...data, bannerStatus: BannerStatus.Uploaded };
    } else {
        return data.totalFilesSize > maxSize
            ? { ...data, bannerStatus: BannerStatus.Error, details: 'Total files size is too big!' }
            : { ...data, bannerStatus: BannerStatus.Uploaded };
    }
};

export const getBannerSizeByHTML = (html: string): Size => {
    const width = Number((html.match(/<meta.+?width=(\d+).+/) ?? [])[1]);
    const height = Number((html.match(/<meta.+?height=(\d+).+/) ?? [])[1]);

    if (isNaN(width) || width === 0 || isNaN(height) || height === 0) {
        throw new Error('Can\'t calculate banner size by media!');
    }
    return { width, height };
};

export const compressBanner = (
    files: Record<string, ParsedPath[]>,
    pathAndName: string,
    { backupImage, level }: CompressBannerOptions
) => {
    const backupPath = backupImage ? format(backupImage) : '';
    const out = parse(pathAndName).dir;
    const filesList = flatten(Object.values(files))
        .map(parsed => ({
            parsed, path: format(parsed)
        }))
        .filter(pipe(propEq('path', backupPath), not));

    const innerFileName = backupImage
        ? pathAndName
        : join(out, TARGET_ARCHIVE_FILE_NAME);

    return Promise
        .all([
            compress(filesList.map(prop('path')), out, innerFileName, level),
            getFilesSize(out, filesList.map(prop('parsed')))
        ])
        .then(([archiveSize, filesSizes]) => {
            const promise: Promise<void | number> = backupPath
                ? compress([backupPath, innerFileName], out, join(out, TARGET_ARCHIVE_FILE_NAME))
                : Promise.resolve();

            return promise.then(() => ({
                filesSizes,
                totalFilesSize: Object.values(filesSizes).reduce(add, 0),
                archiveSize
            }));
        });
};

export type CompressBannerOptions = {
    level?: number;
    backupImage?: ParsedPath | null;
}

export const minifyHTML = (html: string) => minify(html, {
    minifyCSS: true,
    minifyJS: true,
    minifyURLs: true,
    removeComments: true,
    removeRedundantAttributes: true,
    collapseWhitespace: true,
    removeScriptTypeAttributes: true
});

export type Platform = {
    title: string;
    libTag: string | undefined;
    createjsLibTag: string | undefined;
    metaTags: string | undefined;
    exitScript: string | undefined;
    exitTag: string | undefined;
    bodyScript: string | undefined;
    initScript: string | undefined;
    stageReadyScript: string | undefined;
    maxSize: number | undefined;
    inArchive: boolean | undefined;
}

export type PrepareProcessingData = {
    success: boolean;
    bannerSize: Size;
    filesSizes: Record<string, number>;
    totalFilesSize: number;
    archiveSize: number;
    bannerStatus: BannerStatus;
    details: string;
    platform: string;
    file: File;
}

export type PlatformStringField = {
    [Key in keyof Platform]: Platform[Key] extends string | undefined ? Key : never;
}[keyof Platform];
