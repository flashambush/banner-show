import { format, join, parse } from 'path';
import { equals, pipe } from 'ramda';
import { prepareHtml } from './htmlProcessing';
import { File } from 'formidable';
import {
    addBannerStatus,
    compressBanner,
    compressImages,
    compressJs,
    extract,
    findPlatform,
    getBackupImageFile,
    getBannerSizeByHTML,
    getFilesHashByFolder,
    getHTMLCode,
    getImageSize,
    makeOutDirectory,
    PrepareProcessingData,
    replaceHtml,
    validateHTMLCount
} from './helpers';
import { getGoogleSheets } from '../services/loadMakeBannerSchemas';
import { roundTo } from '../utils/utils';
import { info, warn } from '@tsigel/logger';

const TARGET_HTML_FILE_NAME = 'index.html';

const log = (message: string) =>
    <T>(data: T): T => {
        info(message);
        return data;
    };

export const processBanner = (file: File, out: string, platform: string, client: string, campaign: string): Promise<PrepareProcessingData> => {
    const start = Date.now();
    return makeOutDirectory(out)
        .then(log('Banner processing: Out directory created.'))
        .then(() => extract(file.path, out))
        .then(log('Banner processing: Archive extracted.'))
        .then(() => Promise.all([
            getFilesHashByFolder(out)
                .then(log('Banner processing: Current folder hashMap.')),
            getGoogleSheets()
                .then(findPlatform(platform))
                .then(log('Banner processing: Google table loaded.')),
            compressImages(out)
                .then(log('Banner processing: Images compressed.')),
            compressJs(out)
                .then(log('Banner processing: Js compressed.'))
        ]))
        .then(([files, bannerPlatform]) => {
            const htmlFile = validateHTMLCount(files);
            const backupImage = getBackupImageFile(files);

            if (!backupImage) {
                warn('Has no backup image in banner!');
            }

            log('Start read html file for replace.');
            return Promise
                .all([
                    getHTMLCode(htmlFile)
                        .then<string>(pipe(
                            prepareHtml(bannerPlatform),
                            replaceHtml(TARGET_HTML_FILE_NAME, out, htmlFile)
                        )),
                    backupImage ? getImageSize(format(backupImage)) : Promise.resolve(null)
                ])
                .then(([html, size]) => {
                    log('Html file replaced!');
                    const bannerSize = getBannerSizeByHTML(html);

                    files['.html'] = [parse(join(out, TARGET_HTML_FILE_NAME))];

                    if (size && !equals(bannerSize, size)) {
                        warn('Banner size and image backup size is not equals!'); //TODO task-21
                    }

                    return compressBanner(
                        files,
                        join(out, `${client}_${campaign}_${bannerSize.width}x${bannerSize.height}.zip`),
                        { backupImage }
                    )
                        .then((data) => ({
                            ...data,
                            bannerSize,
                            platform,
                            file
                        }));
                })
                .then(data => {
                    const details = `Banner prepare time ${roundTo((Date.now() - start) / 1000, 2)}s.`;
                    return addBannerStatus({ ...data, details, success: true }, bannerPlatform);
                });
        });
};
