import { curry, nthArg, replace } from 'ramda';
import { getBannerSizeByHTML, Platform, PlatformStringField } from './helpers';
import { Size } from '../server/middlewares/mediaPlan/types';

const REPLACE_PATTERNS: Array<Tag> = [
    {
        name: 'CREATEJS_LIBRARY',
        key: 'createjsLibTag',
        pattern: /\<!--\s*CREATEJS_LIBRARY_START\s*--\>((.|\n)+?)\<!--\s*CREATEJS_LIBRARY_END\s*--\>/g
    },
    {
        name: 'LIBS',
        key: 'libTag',
        pattern: '<!-- LIBS -->'
    },
    {
        name: 'META',
        key: 'metaTags',
        pattern: '<!-- META -->'
    },
    {
        name: 'EXIT',
        key: 'exitScript',
        pattern: '<!-- EXIT -->'
    },
    {
        name: 'EXIT_HREF',
        key: 'exitTag',
        pattern: /\<!--\s*EXIT_HREF_START\s*--\>((.|\n)+?)\<!--\s*EXIT_HREF_END\s*--\>/g
    },
    {
        name: 'JS',
        key: 'bodyScript',
        pattern: '<!-- JS -->'
    }
    ,
    {
        name: 'JS-INIT',
        key: 'initScript',
        pattern: '<!-- JS-INIT -->'
    },
    {
        name: 'JS-STAGE-READY',
        key: 'stageReadyScript',
        pattern: '<!-- JS-STAGE-READY -->'
    }
];

export const prepareHtml = curry((platform: Platform, html: string): string => {
    const bannerSize = getBannerSizeByHTML(html);
    return REPLACE_PATTERNS.reduce((acc, tag) => {
        return replace(tag.pattern, replaceOn(tag, platform, bannerSize), acc);
    }, html);
});

const replaceSizes = (str: string, width: number, height: number): string => str.replace(/\$WT/g, width.toString()).replace(/\$HT/g, height.toString());

const replaceOn = (tag: typeof REPLACE_PATTERNS[0], platform: Platform, bannerSize: Size) => {
    const prop: string | undefined = platform[tag.key];
    switch (tag.name) {
        case 'EXIT':
            return `function Exit() { ${prop} }`;
        case 'META':
            return prop ? replaceSizes(prop, bannerSize.width, bannerSize.height) : '';
        case 'CREATEJS_LIBRARY':
            return prop ? prop : nthArg(1);
        case 'EXIT_HREF':

            return prop ?
                prop :
                // @ts-ignore
                (match: string, p1: string) =>
                    platform['exitScript'] ?
                        replaceSizes(replace('<!-- EXIT_HREF -->', 'javascript:Exit()', p1), bannerSize.width, bannerSize.height)
                        : '';
        default:
            return prop ? prop : '';
    }
};

type Tag = {
    name: string;
    key: PlatformStringField;
    pattern: RegExp | string
}
