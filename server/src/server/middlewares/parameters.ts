import { getValidationDetails, Schema, validate } from '../../utils/validate';
import { Middleware } from 'koa';
import { path } from 'ramda';
import { BadRequest } from '../errors';
import { ObjectSchema, ValidationError } from 'yup';

export type ParamsState<Param> = {
    s_parameters: Param
}

// TODO Remove
export const parametersFromBody = <T extends Schema<any>>(schema: T, from: 'body' | 'params' = 'body'): Middleware<ParamsState<T extends Schema<infer R> ? R : never>> => {
    const validator = validate(schema);

    return (ctx, next) => {
        const body = path(['request', from], ctx);
        const errors = validator(body);

        if (errors.length) {
            return Promise.reject(new BadRequest(getValidationDetails(errors)));
        }

        ctx.state.s_parameters = body as any;

        return next();
    };
};

export const validateByYupM = <T extends ObjectSchema<any>, R extends 'body' | 'query' | 'params'>(schema: T, from: R): Middleware<any, { [Key in R]: T['__outputType'] }> => {
    return ((ctx, next) =>
        schema.validate(ctx[from], { abortEarly: false })
            .catch((details: ValidationError) => {
                const errorHash = details.inner.reduce(
                    (acc, item) =>
                        Object.assign(acc, { [item.path ?? item.name]: item.message }),
                    Object.create(null));

                return Promise.reject(new BadRequest(JSON.stringify(errorHash, null, 4)));
            })
            .then(() => next())) as Middleware<any, { [Key in R]: T['__outputType'] }>;
};
