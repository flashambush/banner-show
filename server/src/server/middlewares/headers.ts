import { Middleware, DefaultState, DefaultContext } from 'koa';

export const setHeadersM: (headers: Record<string, string>) => Middleware =
    (headers: Record<string, string>) =>
        (ctx, next) => {
            Object.entries(headers).forEach(([name, value]) => ctx.set(name, value));
            return next();
        };

export type HeadersState<T extends string> = {
    s_headers: {
        [Key in T]: string
    }
} & DefaultState;
export const getHeadersM = <T extends string>(names: Array<string>): Middleware<HeadersState<T>, DefaultContext> =>
    (ctx, next) => {
         ctx.state.s_headers =
             names
                 .reduce((acc, name: string) => Object.assign(acc, {[name]: ctx.get(name)}), Object.create(null));
         return next();
    }
