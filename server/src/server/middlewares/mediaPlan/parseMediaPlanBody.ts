import { ParameterizedContext } from 'koa';
import { CreateMediaPlan, CreateMediaPlanState } from './types';
import { File } from 'formidable';
import { toArray } from '../../../utils/utils';
import { array, object, string, ValidationError } from 'yup';
import { BadRequest } from '../../errors';

const schema = object().shape({
    client: string().required(),
    brand: string().required(),
    campaign: string().required(),
    banners: array().required().min(1).of(object().shape({
        platform: string().required(),
        file: object().required()
    }))
});

export const parseMediaPlanBody = (ctx: ParameterizedContext<CreateMediaPlanState>) => {
    const files = Object.values<File | Array<File>>((ctx.request.files ?? {}) as any)
        .map((item: File | Array<File>) => toArray(item));

    const form: CreateMediaPlan<File> = Object.entries<string>(ctx.request.body ?? {})
        .reduce<CreateMediaPlan<File>>((acc, [name, value]) => {
            switch (name) {
                case 'client':
                    acc.client = value;
                    break;
                case 'brand':
                    acc.brand = value;
                    break;
                case 'campaign':
                    acc.campaign = value;
                    break;
                default:
                    if (/banner\.\d+\.platform/.test(name)) {
                        const index = Number((name.match(/(\d+)/) ?? [])[1]);
                        if (!acc.banners) {
                            acc.banners = [];
                        }
                        acc.banners.push(...files[index].map((file => ({
                            file,
                            platform: value
                        }))));
                    }

            }
            return acc;
        }, Object.create(null));

    return schema.validate(form, { abortEarly: false })
        .catch((details: ValidationError) => {
            const errorHash = details.inner.reduce(
                (acc, item) =>
                    Object.assign(acc, { [item.path ?? item.name]: item.message }),
                Object.create(null));

            return Promise.reject(new BadRequest(JSON.stringify(errorHash, null, 4)));
        })
        .then<CreateMediaPlan<File>>((data) => data as unknown as CreateMediaPlan<File>);
};