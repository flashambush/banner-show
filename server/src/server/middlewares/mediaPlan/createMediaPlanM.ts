import { CreateMediaPlanState } from './types';
import { makeTmpExtractFolderPath } from '../../../utils/utils';
import Router from 'koa-router';
import { ProcessPack } from '../../../services/ProcessPack';
import { Queue } from '../../../services/Queue';
import { Process } from '../../../services/Process';
import { processBanner } from '../../../processBanner/processBanner';
import { PrepareProcessingData } from '../../../processBanner/helpers';
import { MediaPlan } from '../../../db/models/MediaPlan';
import { BANNERS_DIR } from '../../../constants';
import { join } from 'path';
import { promises } from 'fs';
import { parseMediaPlanBody } from './parseMediaPlanBody';
import { userRequestManager } from '../../UserRequestManager';
import { asyncMap } from '@tsigel/async-map';
import { BannerType } from '../../../../../types/types';
import { getCreateMediaPlanId, makeProcessId } from './utils';


export const createMediaPlanM: (queue: Queue<PrepareProcessingData>) => Router.IMiddleware<CreateMediaPlanState> =
    (queue) =>
        (ctx, next) =>
            parseMediaPlanBody(ctx)
                .then((mediaPlan) => {
                    const id = getCreateMediaPlanId(mediaPlan);

                    const processList = mediaPlan.banners.map(({ file, platform }) =>
                        new Process(
                            makeProcessId(id, file, platform),
                            () =>
                                processBanner(
                                    file,
                                    makeTmpExtractFolderPath(file),
                                    platform,
                                    mediaPlan.client,
                                    mediaPlan.campaign
                                )
                        )
                    );

                    const pack = new ProcessPack<PrepareProcessingData>(queue as Queue<PrepareProcessingData>, id, processList);

                    userRequestManager.addJob({
                        author: ctx.state.s_user,
                        id,
                        processPack: pack
                    });

                    pack.once('finish', list => {
                        const data = list.filter(item => item.success)
                            .map(item => item.data as PrepareProcessingData);

                        MediaPlan
                            .create({
                                client: mediaPlan.client,
                                brand: mediaPlan.brand,
                                campaign: mediaPlan.campaign,
                                author: ctx.state.s_user.id,
                                banners: data.map((data) => ({
                                    width: data.bannerSize.width,
                                    height: data.bannerSize.height,
                                    type: BannerType.Html,
                                    platform: data.platform,
                                    bannerStatus: data.bannerStatus,
                                    version: 1,
                                    details: data.details
                                }))
                            })
                            .then((mediaPlan) => {
                                return asyncMap(1, (item, index) => {
                                    const oldPath = makeTmpExtractFolderPath(data[index].file);
                                    const newPath = join(BANNERS_DIR, item.banner_id);

                                    return promises.rename(oldPath, newPath);
                                }, mediaPlan.banners);
                            });
                    });

                    ctx.body = {
                        type: 'process-run',
                        id: pack.id
                    };

                    return next();
                });
