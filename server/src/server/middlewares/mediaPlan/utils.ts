import { CreateMediaPlan } from './types';
import { File } from 'formidable';

export const getCreateMediaPlanId = (mediaPlan: CreateMediaPlan<File>) =>
    `${mediaPlan.campaign}-${mediaPlan.client}-${mediaPlan.brand}-${mediaPlan.banners.length}`;

export const makeProcessId = (id: string, file: File, platform: string) =>
    `${id}-${platform}-${file.name}`
