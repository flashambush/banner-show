import { File } from 'formidable';
import { AuthState } from '../auth/types';

export type CreateMediaPlanFormState = {
    s_form: CreateMediaPlan<File>;
};

export type CreateMediaPlanDataState = {
    s_mediaPlanData: CreateMediaPlan<PrepareOut>;
}

export type CreateMediaPlan<T> = {
    client: string;
    brand: string;
    campaign: string;
    banners: Array<MediaPlanBannerItem<T>>;
};

export type MediaPlanBannerItem<T> = {
    platform: string;
    file: T;
}

export type PrepareOut = {
    size: Size;
    totalSize: number,
    sizeByFiles: Record<string, number>;
    zipSize: number;
    extractedFolderPath: string;
    file: File
}

export type Size = {
    width: number;
    height: number;
}

export type CreateMediaPlanState =
    AuthState & CreateMediaPlanFormState & CreateMediaPlanDataState;
