import { Middleware } from 'koa';
import { User } from '../../../db/models/User';
import moment, { DurationInputArg2 } from 'moment';
import { base64Encode, signBytes } from '@waves/ts-lib-crypto';
import { Token } from '../../../protobuf/protos';
import { pick } from 'ramda';
import { getTokenBytes } from './utils';
import { SECRET } from '../../../constants';
import { BadRequest } from '../../errors';
import { isString, required } from '../../../utils/validators';
import { parametersFromBody, ParamsState } from '../parameters';
import { Schema } from '../../../utils/validate';

type OauthParams = {
    username: string;
    password: string;
}

const oauthSchema: Schema<OauthParams> = {
    username: [
        required,
        isString
    ],
    password: [
        required,
        isString
    ]
};

const createTokenM: Middleware<ParamsState<OauthParams>> = (ctx, next) => {
    return User.getByUniqueProperty('name', ctx.state.s_parameters.username)
        .then((user) => {
            if (!user) {
                return Promise.reject(new BadRequest('User not found!'));
            }

            if (user.password !== ctx.state.s_parameters.password) {
                return Promise.reject(new BadRequest('Wrong password!'));
            }

            const accessTokenExpiredAt = moment()
                .add(process.env.TOKEN_LIVE_VALUE, process.env.TOKEN_LIVE_PERIOD as DurationInputArg2)
                .toDate()
                .getTime();

            const accessTokenBytes = getTokenBytes(accessTokenExpiredAt, 'manager', user.id);
            const accessTokenSignature = signBytes(SECRET, accessTokenBytes);
            const token = base64Encode(Token.encode({
                expiredAt: accessTokenExpiredAt,
                scope: 'manager',
                signature: accessTokenSignature,
                type: Token.TYPE.ACCESS,
                userId: user.id
            }).finish());

            ctx.body = {
                type: 'token',
                token,
                accessTokenExpiredAt,
                user: pick(['name', 'email'], user)
            };

            return next();
        });
};

export const oauthM: Array<Middleware<ParamsState<OauthParams>>> = [
    parametersFromBody(oauthSchema),
    createTokenM
];
