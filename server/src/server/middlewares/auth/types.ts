import { HeadersState } from '../headers';
import { User } from '../../../db/models/User';

export type AuthState = HeadersState<'Authorization'> & {
    s_user: User;
}