import { Middleware } from 'koa';
import { getHeadersM } from '../headers';
import { AuthError } from '../../errors';
import { parseToken } from './utils';
import { AuthState } from './types';


const checkAuth: Middleware<AuthState> = (ctx, next) => {
    if (!ctx.state.s_headers.Authorization) {
        return Promise.reject(new AuthError());
    }
    return parseToken(ctx.state.s_headers.Authorization)
        .then((user) => {
            ctx.state.s_user = user;
            return next();
        });
};

export const checkTokenM: Array<Middleware<AuthState>> = [
    getHeadersM<'Authorization'>(['Authorization']),
    checkAuth
];

export const authM: Middleware<AuthState> = (ctx, next) => {
    ctx.body = ctx.state.s_user.getClientData();
    return next();
};
