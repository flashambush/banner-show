import { BigNumber } from '@waves/bignumber';
import { base64Decode, publicKey, stringToBytes, verifySignature } from '@waves/ts-lib-crypto';
import { Token } from '../../../protobuf/protos';
import { AuthError } from '../../errors';
import { SECRET } from '../../../constants';
import { User } from '../../../db/models/User';


export const parseToken = (tokenBase64: string): Promise<User> => {
    try {
        const token = Token.decode(base64Decode(tokenBase64));
        const bytes = getTokenBytes(Number(token.expiredAt.toString()), token.scope, token.userId);
        const isExpired = Date.now() > Number(token.expiredAt.toString());

        if (isExpired) {
            return Promise.reject(new AuthError('Token expired!'));
        }

        const isValidSignature = verifySignature(publicKey(SECRET), bytes, token.signature);

        if (!isValidSignature) {
            return Promise.reject(new AuthError('Invalid token signature!'));
        }

        return User.getByUniqueProperty('id', token.userId)
            .then((user) => {
                if (!user) {
                    return Promise.reject('Wrong token!');
                }
                return user;
            })
    } catch (e) {
        return Promise.reject(new AuthError('Invalid token!'));
    }
}

export const getTokenBytes = (expireAt: number, scope: string, userId: number) => {
    const expireBytes = new BigNumber(expireAt).toBytes({ isSigned: false, isLong: true });
    const userIdBytes = new BigNumber(userId).toBytes({ isSigned: false, isLong: true });
    const scopeBytes = stringToBytes(scope, 'utf8');

    return Uint8Array.from([
        ...Array.from(expireBytes),
        ...Array.from(userIdBytes),
        ...Array.from(scopeBytes),
    ]);
};
