import Router from 'koa-router';
import { authM, checkTokenM } from '../middlewares/auth/checkTokenM';
import { oauthM } from '../middlewares/auth/oauthM';
import { createMediaPlanM } from '../middlewares/mediaPlan/createMediaPlanM';
import { MediaPlan } from '../../db/models/MediaPlan';
import { Banner } from '../../db/models/Banner';
import { validateByYupM } from '../middlewares/parameters';
import { number, object, string } from 'yup';
import { PrepareProcessingData } from '../../processBanner/helpers';
import { Queue } from '../../services/Queue';

export const createRouter = (queue: Queue<PrepareProcessingData>) => {
    const router = new Router();
    const privateRouter = new Router();

    const mediaPlanIdSchema = object().shape({
        id: number().required()
    });

    const bannerIdSchema = object().shape({
        banner_id: string().required()
    });

    router
        .post('/token', ...oauthM)
        .get('/public/banners/:banner_id',
            validateByYupM(bannerIdSchema, 'params'),
            Banner.getByIdM({ approvedOnly: true })
        )
        .get('/public/mediaPlan/:id', MediaPlan.getByIdM())
        .get('/public/mediaPlan/:id/banners',
            validateByYupM(mediaPlanIdSchema, 'params'),
            Banner.getByMediaPlanIdM({ approvedOnly: true })
        );

    privateRouter
        .use(...checkTokenM)
        .get('/user', authM)
        .post('/mediaPlan/create', createMediaPlanM(queue))
        .get('/mediaPlan/list', MediaPlan.getListM())
        .get('/mediaPlan/:id/banners',
            validateByYupM(mediaPlanIdSchema, 'params'),
            Banner.getByMediaPlanIdM()
        )
        .get(
            '/banners/:banner_id',
            validateByYupM(bannerIdSchema, 'params'),
            Banner.getByIdM()
        )
        .post('/banners/:banner_id',
            validateByYupM(bannerIdSchema, 'params'),
            Banner.updateM(queue)
        )
        .post('/banners/:banner_id/approve',
            validateByYupM(bannerIdSchema, 'params'),
            Banner.approveM()
        )
        .delete('/banners/:banner_id',
            validateByYupM(bannerIdSchema, 'params'),
            Banner.deleteM()
        );

    return {
        router,
        privateRouter
    };
};


