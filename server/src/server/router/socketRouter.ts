import { socketManager } from '../soket/SocketManager';
import { parseToken } from '../middlewares/auth/utils';
import { pick } from 'ramda';
import { info } from '@tsigel/logger';

export const createSocketRouter = () => {
    socketManager.addRequestListener('auth', (request, _store, setStore) => {
        info(`Auth socket request!`);
        return parseToken(request.body)
            .then(user => {
                setStore({ user });
                info(`Success socket auth for user with name ${user.name}`);
                return pick(['name', 'email'], user);
            });
    });
};
