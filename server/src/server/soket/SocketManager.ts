import { Socket } from 'socket.io';
import { User } from '../../db/models/User';
import { equals, findIndex, pipe, propEq } from 'ramda';
import { error, info, warn } from '@tsigel/logger';
import { AuthError, BadRequest, InternalError, NotFound, ServerError } from '../errors';
import { ProcessPack } from '../../services/ProcessPack';


export class SocketManager {
    private static AVAILABLE_TOPICS: Array<Topic> = ['bannersPrepareDone', 'bannersPrepareProgress'];
    private sockets: Array<Socket> = [];
    private stores: Record<string, SocketStore> = Object.create(null);
    private requestListeners: Record<keyof SocketRequestMap, RequestHandlerData> = Object.create(null);
    private subscribers: Record<string, Array<string>> = Object.create(null);

    public static makeResponseError<T extends keyof SocketRequestMap>(request: SocketRequestMap[T]['request'], error: ServerError): SocketResponseError<T> {
        warn(`Socket error: ${error.message}, code: ${error.code}`);
        return {
            url: request.url as T,
            body: error,
            ok: false,
            status: error.code,
            requestId: request.requestId
        };
    }

    public addRequestListener<T extends keyof SocketRequestMap>(event: T, handler: SocketHandler<T>, isPrivate: boolean = false): this {
        if (!this.requestListeners[event]) {
            this.requestListeners[event] = { handler, isPrivate };
        } else {
            error(`Duplicate request handler! "${event}"`);
            throw new Error(`Duplicate request handler! "${event}"`);
        }
        return this;
    }

    public sendMessageForUser<T extends Topic>(userId: number, message: SocketClientNotify<T, any>): this {
        const [socketId] = Object.entries(this.stores)
            .find(([_, store]) => equals(store?.user?.id, userId)) || [];

        if (!socketId) {
            return this;
        }

        const socket = this.sockets.find(propEq('id', socketId));

        if (!socket) {
            warn(`Has no socket with id ${socketId} for user with id ${userId}`);
            return this;
        }

        if (!this.subscribers[socket.id] || !this.subscribers[socket.id].includes(message.topic)) {
            info(`Has no subscribers for this topic ${message.topic}`);
            return this;
        }

        socket.emit(message.topic, message.body);

        return this;
    }

    public getConnectHandler() {
        return (socket: Socket) => {
            info(`Connect new socket with id ${socket.id}.`);

            socket.once('disconnect', () => {
                info(`Socket with id ${socket.id} disconnected!`);

                const index = findIndex(propEq('id', socket.id), this.sockets);

                if (index !== -1) {
                    this.sockets.splice(index, 1);
                }

                delete this.stores[socket.id];
                delete this.subscribers[socket.id];
                socket.offAny();
            });

            this.sockets.push(socket);

            socket.on('request', this.makeRequestHandler(socket));
            socket.on('subscribe', this.makeSubscribeHandler(socket));
            socket.on('unsubscribe', this.makeUnsubscribeHandler(socket));
        };
    }

    private makeUnsubscribeHandler(socket: Socket) {
        return ({ topic }: SocketSubscribeData) => {
            const sendOk = () => {
                socket.emit('unsubscribe-response', { ok: true });
            };

            const sendError = (error: Error) => {
                if (error instanceof ServerError) {
                    socket.emit('unsubscribe-response', {
                        ok: false,
                        message: error.message
                    });
                } else {
                    socket.emit('unsubscribe-response', {
                        ok: false,
                        message: new InternalError(error.message).message
                    });
                }
            };

            if (!this.subscribers[socket.id] || !this.subscribers[socket.id].includes(topic)) {
                sendError(new NotFound());
                return void 0;
            }

            const index = this.subscribers[socket.id].indexOf(topic);
            this.subscribers[socket.id].splice(index, 1);

            sendOk();
        }
    }

    private makeSubscribeHandler(socket: Socket) {
        return ({ topic }: SocketSubscribeData) => {

            const sendOk = () => {
                socket.emit('subscribe-response', { ok: true });
            };

            const sendError = (error: Error) => {
                if (error instanceof ServerError) {
                    socket.emit('subscribe-response', {
                        ok: false,
                        message: error.message
                    });
                } else {
                    socket.emit('subscribe-response', {
                        ok: false,
                        message: new InternalError(error.message).message
                    });
                }
            };

            if (!this.subscribers[socket.id]) {
                this.subscribers[socket.id] = [];
            }

            if (!SocketManager.AVAILABLE_TOPICS.includes(topic as Topic)) {
                sendError(new NotFound());
                return void 0;
            }

            if (this.subscribers[socket.id].includes(topic)) {
                sendError(new BadRequest('Duplicate topic subscribe!'));
                return void 0;
            }

            this.subscribers[socket.id].push(topic);
            sendOk();
        };
    }

    private makeRequestHandler(socket: Socket) {
        return (data: SocketRequestMap[keyof SocketRequestMap]['request']) => {
            info(`Request: ${JSON.stringify(data, null, 4)}`);

            const send = (response: SocketRequestMap[keyof SocketRequestMap]['response']) => {
                socket.emit('response', response);
            };

            const makeSuccessResponse = (body: SocketRequestMap[keyof SocketRequestMap]['response']['body']): SocketRequestMap[keyof SocketRequestMap]['response'] => ({
                ok: true,
                body,
                requestId: data.requestId,
                url: data.url,
                status: 200
            } as SocketRequestMap[keyof SocketRequestMap]['response']);

            if (!data.url) {
                send(SocketManager.makeResponseError(data, new BadRequest('Has no request url!')));
                return void 0;
            }

            if (!data.requestId) {
                send(SocketManager.makeResponseError(data, new BadRequest('Has no requestId!')));
                return void 0;
            }

            if (!this.requestListeners[data.url]) {
                send(SocketManager.makeResponseError(data, new NotFound()));
                return void 0;
            }

            if (!this.stores[socket.id]) {
                this.stores[socket.id] = Object.create(null);
            }

            const store = this.stores[socket.id];

            const { handler, isPrivate } = this.requestListeners[data.url];

            if (isPrivate && !store.user) {
                send(SocketManager.makeResponseError(data, new AuthError('Unauthorized')));
                return void 0;
            }

            const setStore = (store: SocketStore) => {
                this.stores[socket.id] = store;
            };

            try {
                handler(data, { ...store }, setStore)
                    .then(
                        pipe(makeSuccessResponse, send),
                        (error: Error) => {
                            if (error instanceof ServerError) {
                                return send(SocketManager.makeResponseError(data, error));
                            } else {
                                return send(SocketManager.makeResponseError(data, new InternalError(error.message)));
                            }
                        });
            } catch (e) {
                send(SocketManager.makeResponseError(data, new InternalError(e.message)));
            }
        };
    }
}

type RequestHandlerData = {
    handler: SocketHandler<any>;
    isPrivate: boolean;
}

type SocketRequest<RequestURL extends string, Body> = {
    url: RequestURL;
    body: Body;
    requestId: string;
    user?: User;
}

type SocketResponseSuccess<RequestURL, Body> = {
    ok: true;
    status: 200;
    url: RequestURL;
    body: Body;
    requestId: string;
}

type SocketResponseError<RequestURL> = {
    ok: false;
    status: number;
    url: RequestURL;
    requestId: string;
    body: ServerError;
}

type SocketStore = {
    user?: User;
}

type RequestPair<RequestURL extends string, RequestBody, ResponseBody> = {
    request: SocketRequest<RequestURL, RequestBody>;
    response: SocketResponseSuccess<RequestURL, ResponseBody> | SocketResponseError<RequestURL>;
}

type SocketRequestMap = {
    'auth': RequestPair<'auth', string, Pick<User, 'email' | 'name'>>;
}

type SocketClientNotify<Topic, Body> = {
    topic: Topic;
    body: Body;
}

type SocketMessages =
    SocketClientNotify<'bannersPrepareProgress', ProcessPack.ProgressEvent> |
    SocketClientNotify<'bannersPrepareDone', { id: string }>;

type Topic<Notify extends SocketClientNotify<any, any> = SocketMessages> =
    Notify['topic'];

type SocketSubscribeData = {
    topic: string;
}

type SetStore = (data: SocketStore) => void;

type SocketHandler<Event extends keyof SocketRequestMap> =
    (request: SocketRequestMap[Event]['request'], store: SocketStore, setStore: SetStore) => Promise<SocketRequestMap[Event]['response']['body']>;

export const socketManager = new SocketManager();
