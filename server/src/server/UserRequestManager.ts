import { User } from '../db/models/User';
import { ProcessPack } from '../services/ProcessPack';
import { PrepareProcessingData } from '../processBanner/helpers';
import { socketManager } from './soket/SocketManager';
import { equals, pipe, prop } from 'ramda';

class UserRequestManager {
    private jobs: Array<Job> = [];

    public addJob(job: Job): void {
        this.jobs.push(job);

        job.processPack.on('progress', (event) => {
            socketManager.sendMessageForUser(job.author.id, {
                topic: 'bannersPrepareProgress',
                body: event
            });
        });

        job.processPack.once('finish', () => {
            job.processPack.off();
            this.jobs.splice(this.jobs.indexOf(job), 1);
            socketManager.sendMessageForUser(job.author.id, {
                topic: 'bannersPrepareDone',
                body: {
                    id: job.id
                }
            });
        });
    }

    public getInitialMessageByUser(userId: number): Array<Job> {
        return this.jobs.filter(
            pipe(
                prop('author'),
                prop('id'),
                equals(userId)
            )
        );
    }
}

type Job = {
    author: User;
    id: string;
    processPack: ProcessPack<PrepareProcessingData>
}

export const userRequestManager = new UserRequestManager();
