import Koa from 'koa';
import koaBody from 'koa-body';
import createStaticServer from 'koa-static';
import mount from 'koa-mount';
import { BANNER_PREPARE_TREADS, BANNERS_DIR, TMP_FILES_DIR } from '../constants';
import { setHeadersM } from './middlewares/headers';
import { createRouter } from './router';
import { applyErrorM } from './errors';
import { Queue } from '../services/Queue';
import { PrepareProcessingData } from '../processBanner/helpers';
import { createServer } from 'http';
import { Server } from 'socket.io';
import { socketManager } from './soket/SocketManager';
import { info } from '@tsigel/logger';
import { createSocketRouter } from './router/socketRouter';

const staticServer = new Koa();
const queue = new Queue<PrepareProcessingData>(BANNER_PREPARE_TREADS);

const { router, privateRouter } = createRouter(queue);

staticServer
    .use(setHeadersM({
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Headers': 'Content-Type'
    }))
    .use(createStaticServer(BANNERS_DIR, {
        maxage: 60 * 5,
    }));

const apiServer = new Koa();

apiServer
    .use(koaBody({
        multipart: true,
        formidable: {
            uploadDir: TMP_FILES_DIR
        },
        json: true
    }))
    .use(setHeadersM({
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Headers': 'Content-Type,Authorization',
        'Access-Control-Allow-Methods': 'PUT,DELETE,GET,POST,OPTIONS,UPDATE'
    }))
    .use(applyErrorM)
    .use(router.routes())
    .use(router.allowedMethods())
    .use(privateRouter.routes())
    .use(privateRouter.allowedMethods());

export const launchServer = (app: Koa<Koa.DefaultState, Koa.DefaultContext>) => {
    app
        .use(mount('/api', apiServer))
        .use(mount('/banners/view', staticServer));

    createSocketRouter();

    const REST_PORT = process.env.REST_PORT;
    const SOCKET_PORT = process.env.SOCKET_PORT;
    const restServer = createServer(app.callback());

    const socketServer = REST_PORT === SOCKET_PORT ? restServer : createServer();
    const io: Server = new Server(socketServer);

    io.on('connect', socketManager.getConnectHandler());

    restServer
        .listen(REST_PORT);

    if (REST_PORT !== SOCKET_PORT) {
        socketServer
            .listen(SOCKET_PORT);
    }

    info(`Server listen ${REST_PORT} port.`);
    info(`Listen socket on ${SOCKET_PORT} port.`);
};
