import React, { Fragment, lazy, useEffect, useState, Suspense } from 'react';
import './App.styl';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import { MediaPlanCreate } from './pages/MediaPlan/MediaPlanCreate';
import { MediaPlansList } from './pages/MediaPlan/MediaPlansList';
import { MediaPlanView } from './pages/MediaPlan/MediaPlanView';
import { PrivateRoute } from './auth/PrivateRoute';
import { Login } from './pages/Login/Login';
import { useAuth } from './auth/useAuth';
import { always } from 'ramda';
import { InnerPage } from './Components/PageWrap/InnerPage';
import { PublicBannerView } from './pages/Public/PublicBannerView';
import { BannerView } from './pages/Banner/BannerView';
import { Loading } from './Components/Loading/Loading';
import { PublicPlatformView } from './pages/Public/PublicPlatformView';
import { socketManager } from './services/socket';
import './models/init';
import { Notifications } from './Components/Notification/Notifications';

const DownloadTemplate = lazy(() => import('./pages/Static/DownloadTemplate'));

export const App = () => {
    const { token, user, signIn } = useAuth();
    const [loading, setLoading] = useState<boolean>(true);

    useEffect(() => {
        if (user) {
            setLoading(false);
            return void 0;
        }

        if (!token) {
            setLoading(false);
            return void 0;
        }

        socketManager.request('auth', token)
            .then((response) => signIn({ token, user: response.body }))
            .catch(always(undefined)) //TODO catch огонь
            .then(() => {
                setLoading(false);
            });
        // eslint-disable-next-line
    }, []);

    return (
        <Fragment>
            {
                loading
                    ? <Loading/>
                    :
                    <Suspense fallback={<Loading/>}>
                        <Notifications/>
                        <BrowserRouter>
                            <Switch>
                                <Route
                                    path={'/login'}
                                    component={Login}/>
                                <Route
                                    path={'/public/:campaignId/:bannerId'}
                                    exact={true}
                                    component={PublicBannerView}/>
                                <Route
                                    path={'/public/:campaignId/:bannerId/:site'}
                                    exact={true}
                                    component={PublicBannerView}/>
                                <Route
                                    path={'/public/:campaignId/'}
                                    exact={true}
                                    component={PublicPlatformView}/>
                                <Route
                                    path={'/public/campaign/:campaignId/platform/:platform'}
                                    exact={true}
                                    component={PublicPlatformView}/>
                                <Route
                                    path={'/public/campaign/:campaignId/platform/:platform/site/:site'}
                                    exact={true}
                                    component={PublicPlatformView}/>
                                <PrivateRoute
                                    defaultUrl={'/login'}
                                    path={'/campaign/:campaignId/banner/:bannerId'}
                                    exact={true}>
                                    <InnerPage page={BannerView}/>
                                </PrivateRoute>
                                <PrivateRoute
                                    defaultUrl={'/login'}
                                    path={['/campaign/list', '/']}
                                    exact={true}>
                                    <InnerPage page={MediaPlansList}/>
                                </PrivateRoute>
                                <PrivateRoute
                                    defaultUrl={'/login'}
                                    path={'/campaign/:campaignId/'}
                                    exact={true}>
                                    <InnerPage page={MediaPlanView}/>
                                </PrivateRoute>
                                <PrivateRoute
                                    defaultUrl={'/login'}
                                    path={'/campaign/:campaignId/platform/:platform'}
                                    exact={true}>
                                    <InnerPage page={MediaPlanView}/>
                                </PrivateRoute>
                                <PrivateRoute
                                    defaultUrl={'/login'}
                                    path={'/banner/create'}>
                                    <InnerPage page={MediaPlanCreate}/>
                                </PrivateRoute>
                                <PrivateRoute
                                    defaultUrl={'/login'}
                                    path={'/page/template'}>
                                    <InnerPage page={DownloadTemplate}/>
                                </PrivateRoute>
                                <Redirect to={'/login'}/>
                            </Switch>
                        </BrowserRouter>
                    </Suspense>
            }
        </Fragment>
    );
};
