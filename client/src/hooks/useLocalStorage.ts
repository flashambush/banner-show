import { Dispatch, useCallback, useEffect, useState } from 'react';

export const useLocalStorage = (property: string, initialValue: string): [string, Dispatch<string>] => {
    const [value, setValue] = useState(
        () => window.localStorage.getItem(property) || initialValue
    );

    const setItem = (newValue: string) => {
        setValue(newValue);
        window.localStorage.setItem(property, newValue);
    };

    useEffect(() => {
        const newValue = window.localStorage.getItem(property);
        if (value !== newValue) {
            setValue(newValue || initialValue);
        }
        // eslint-disable-next-line
    }, []);

    const handleStorage = useCallback(
        (event: StorageEvent) => {
            if (event.key === property && event.newValue !== value) {
                setValue(event.newValue || initialValue);
            }
        },
        [value, initialValue, property]
    );

    useEffect(() => {
        window.addEventListener('storage', handleStorage);
        return () => window.removeEventListener('storage', handleStorage);
    }, [handleStorage]);

    return [value, setItem];
};
