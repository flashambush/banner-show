import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle';
import { App } from './App';
import { ProvideAuth } from './auth/ProvideAuth';

ReactDOM.render(
    <React.StrictMode>
        <ProvideAuth>
            <App/>
        </ProvideAuth>
    </React.StrictMode>,
    document.getElementById('root')
);
