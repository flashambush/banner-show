import { ParsedRequest } from '../services/request/request';
import { pipe, prop } from 'ramda';

export const getResponseContent: GetResponseContent = pipe(
    prop('data'),
    prop('content')
);

export type ResponseBody<T> = {
    content: T;
}

export interface GetResponseContent {
    <T extends ParsedRequest<ResponseBody<any>>>(response: T): T['data']['content']
}