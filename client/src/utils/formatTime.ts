import moment from 'moment'

export const formatCreatedDate = (date: string) => {
    return moment(date).format('DD-MM-YYYY HH:mm:ss');
};