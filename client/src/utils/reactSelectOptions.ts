import { Banner } from '../types';
import { groupBy, pipe, prop } from 'ramda';

const makeSelectGroupedOptions = (obj: Record<string, Array<Banner>>): SelectGroupedOptions =>
    Object.entries(obj).reduce((acc: SelectGroupedOptions, [label, list]: [string, Banner[]], i): SelectGroupedOptions => {
        acc.push({ label: label, options: list });
        return acc;
    }, []);

export const optionsGroupedByPlatform = pipe<[Array<Banner>], Record<string, Array<Banner>>, SelectGroupedOptions>(
    groupBy(prop('platform')),
    makeSelectGroupedOptions
);

type SelectGroupedOptions = Array<{ label: string, options: Array<Banner>}>;