import { makeUrl, parse, ParsedRequest } from '../services/request/request';
import { getResponseContent } from './getResponseContent';
import { MediaPlan } from '../types';

export const getCampaign = (campaignId: string | number) => {
    return fetch(makeUrl(`/api/public/mediaPlan/${campaignId}`))
        .then<CampaignResponse>(parse)
        .then(getResponseContent);
};

type CampaignResponse = ParsedRequest<{
    content: MediaPlan
}>;


