import { BASE_URL, SERVER_URL } from '../constants';

export const getPublicDownloadLink = (bannerId: string) => {
    return `${SERVER_URL}/banners/view/${bannerId}/banner.zip`;
};

export const getPublicPreviewLink = (bannerId: string, campaignId: string, isAbsolute = true) => {
    return `${isAbsolute ? BASE_URL : ''}/public/${campaignId}/${bannerId}`;
};

export const getPublicPlatformPreviewLink = (campaignId: string, platform: string, isAbsolute = true) => {
    return `${isAbsolute ? BASE_URL : ''}/public/campaign/${campaignId}/platform/${platform}`;
};

export const getBannerLink = (bannerId: string, campaignId: string) => {
    return `/campaign/${campaignId}/banner/${bannerId}`;
};

export const getPlatformLink = (campaignId: string, platform: string) => {
    return `/campaign/${campaignId}/platform/${platform}`;
};

export const getCreateMediaPlanLink = () => {
    return `/banner/create`;
};

export const downloadResource = (url: string, filename: string) => {
    const unSpacedFileName = filename.replace(/\s/g, '_');
    const forceDownload = (blob: string, filename: string) => {
        const a = document.createElement('a');
        a.download = filename;
        a.style.display = 'none';
        a.href = blob;
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    };

    fetch(url)
        .then(response => response.blob())
        .then(blob => {
            forceDownload(window.URL.createObjectURL(blob), unSpacedFileName);
        })
        .catch(e => console.error(e));
};