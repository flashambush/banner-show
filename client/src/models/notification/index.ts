import { MouseEvent } from 'react';
import { createEvent, createStore } from 'effector';
import { BannerProcessDone, BannerProcessProgress } from '../../../../types/types';
import { NotificationType } from '../../types';
import { NotificationBase, NotificationProgress, NotificationText } from '../../Components/Notification/Notifications';

export const $notification = createStore<Notification>({
    title: '',
    isShown: false,
    time: 0,
    type: NotificationType.Text
});

export const notificationClosed = createEvent<MouseEvent<HTMLButtonElement>>();
export const bannerProgressUpdated = createEvent<BannerProcessProgress>();
export const bannerProgressDone = createEvent<BannerProcessDone>();

type Notification = {
    type: NotificationType;
} & NotificationBase & NotificationText & NotificationProgress;