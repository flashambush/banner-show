import { $notification, bannerProgressDone, bannerProgressUpdated, notificationClosed } from './index';
import { NotificationType } from '../../types';

$notification.on(bannerProgressUpdated, (state, data) => (
    {
        type: NotificationType.Progress,
        title: `Uploading`,
        time: 0,
        progress: data.bannerIndexInProgress,
        total: data.bannersCount,
        isShown: true
    }
));

$notification.on(bannerProgressDone, (state, id) => (
    {
            type: NotificationType.Text,
            message: `All banners uploads successful`,
            time: state.time,
            title: `Upload done!`,
            isShown: true
    }
));

$notification.on(notificationClosed, (state, event) => (
    { ...state, isShown: false }
));
