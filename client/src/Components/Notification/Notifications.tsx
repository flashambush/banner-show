import React from 'react';
import { useStore } from 'effector-react';
import { $notification } from '../../models/notification';
import { NotificationType } from '../../types';
import { NotificationBodyText } from './NotificationBodyText';
import { NotificationBodyProgress } from './NotificationBodyProgress';
import { Notification } from './Notification';

export const Notifications = () => {

    const notification = useStore($notification);

    return (
        <div className="toast-container position-absolute bottom-0 end-0 m-3">
            <Notification title={notification.title}
                          time={notification.time}
                          isShown={notification.isShown}>
                {
                    notification.type == NotificationType.Text ?
                        <NotificationBodyText message={notification.message}/>
                        :
                        <NotificationBodyProgress progress={notification.progress}
                                                  total={notification.total}/>

                }
            </Notification>
        </div>
    );
};

export type NotificationBase = {
    title: string;
    isShown: boolean;
    time: number;
}

export type NotificationText = {
    message?: string | undefined;
}

export type NotificationProgress = {
    progress?: number | undefined;
    total?: number | undefined;
}

