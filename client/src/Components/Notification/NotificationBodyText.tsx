import React, { FC, Fragment } from 'react';
import { NotificationText } from './Notifications';

export const NotificationBodyText: FC<NotificationText> = ({ message = '' }) => (
    <Fragment>
        {message}
    </Fragment>
);


