import React, { FC, useEffect } from 'react';
import cn from 'classnames';
import { notificationClosed } from '../../models/notification';
import { NotificationProgress, NotificationText } from './Notifications';

export const NotificationBodyProgress: FC<NotificationProgress> = ({ progress = 0, total = 0 }) => (
    <div className="progress">
        <div className="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
             aria-valuenow={progress}
             aria-valuemin={0}
             aria-valuemax={total}
             style={{ width: `${progress / total * 100}%` }}/>
    </div>
);


