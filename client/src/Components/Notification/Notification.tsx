import React, { FC, PropsWithChildren } from 'react';
import cn from 'classnames';
import { notificationClosed } from '../../models/notification';
import { NotificationBase } from './Notifications';

export const Notification: FC<PropsWithChildren<NotificationBase>> = ({ children, isShown, title, time }) => (
    <div className={cn('toast', 'fade', isShown ? 'show' : 'hide')}
         role="alert"
         data-bs-autohide={true}
         data-bs-delay={3000}
         aria-live="assertive"
         aria-atomic="true">
        <div className="toast-header">
            <strong className="me-auto">{title}</strong>
            <small className="text-muted">{time}</small>
            <button type="button"
                    className="btn-close"
                    onClick={notificationClosed}
                    aria-label="Close"/>
        </div>
        <div className="toast-body">
            {children}
        </div>
    </div>
);