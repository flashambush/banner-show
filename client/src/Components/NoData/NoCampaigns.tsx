import React from 'react';
import { Link } from 'react-router-dom';
import { getCreateMediaPlanLink } from '../../utils/getURLs';

export const NoCampaigns = () => (
    <div className={'row'}>
        <div className={'col-6 mx-auto text-center'}>
            <h2 className={'pt-5'}>There is no uploaded campagins.</h2>
            <Link
                to={getCreateMediaPlanLink()}
                className={'btn btn-primary btn-lg'}>Create new Mediaplan</Link>
        </div>
    </div>
);