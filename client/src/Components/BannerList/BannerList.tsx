import { Banner } from '../../types';
import { BannersListItem } from './BannersListItem';
import ReactTooltip from 'react-tooltip';
import React, { FC, Fragment, useCallback } from 'react';
import { getBannerLink } from '../../utils/getURLs';
import { useHistory } from 'react-router-dom';
import './bannerList.styl';

export const BannerLsit: FC<Props> = ({ bannerList, campaignId }) => {
    const history = useHistory();

    const onBannerClick = useCallback((e) => {
        history.push(getBannerLink(e.currentTarget.dataset.id, campaignId));
    }, [history, campaignId]);

    return (
        <Fragment>
            <ul className="banner-list">
                {bannerList.map((banner: Banner) =>
                    <BannersListItem key={banner.banner_id}
                                     banner={banner}
                                     onBannerClick={onBannerClick}/>
                )}
            </ul>
            <ReactTooltip type={'info'} delayShow={1000}/>
        </Fragment>
    );
};

type Props = {
    bannerList: Array<Banner>;
    campaignId: string;
}