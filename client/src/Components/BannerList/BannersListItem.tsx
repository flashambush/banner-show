import React, { FC, MouseEventHandler } from 'react';
import { formatCreatedDate } from '../../utils/formatTime';
import { Banner } from '../../types';
import './bannerList.styl';
import cn from 'classnames';

export const BannersListItem: FC<Props> = ({ banner, onBannerClick }) => (
    <li
        onClick={onBannerClick}
        className={cn('banner-list__item', `banner-list__item__status__${banner.bannerStatus}`)}
        data-id={banner.banner_id}
        data-tip={`created at ${formatCreatedDate(banner.createdAt)}`}>
        <div>
            <span>{`${banner.width}x${banner.height}`} {banner.type}</span>
        </div>
    </li>
);

type Props = {
    banner: Banner,
    onBannerClick: MouseEventHandler
}