import React, { FC, Fragment } from 'react';

export const ButtonLoading: FC<{ isLoading: boolean, title: string }> = ({ isLoading, title }) => (
    <Fragment>
        {isLoading ?
            <Fragment>
                <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"/>
                <span className={'ps-2'}>Loading...</span>
            </Fragment>
            :
            <Fragment>{title}</Fragment>
        }
    </Fragment>
);