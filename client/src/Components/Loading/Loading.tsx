import React, { FC, ReactElement } from 'react';

export const Loading: FC<Props> = ({ children, error }) => (
    <div className={'loading'}>{error ? error : 'Loading...'}</div>
);
type Props = {
    children?: ReactElement;
    error?: string | undefined;
}
