import React, { ComponentType, FC, Fragment } from 'react';
import { Navbar } from '../Navbar/Navbar';

export const InnerPage: FC<{ page: ComponentType }> = ({ page: PageComponent }) => {
    return (
        <Fragment>
            <Navbar/>
            <div className={'container-xxl mt-4'}>
                <PageComponent/>
            </div>
        </Fragment>
    );
};