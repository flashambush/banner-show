import React, { FC, ReactNode } from 'react';

export const Breadcrumbs: FC<ReactNode> = ({ children }) => (
    <nav className={'breadcrumbs'} aria-label="breadcrumb">
        <ol className="breadcrumb">
            {React.Children.toArray(children).map((child, index) => (
                <li key={`bc_${index}`} className="breadcrumb-item active" aria-current="page">
                    {child}
                </li>
            ))}
        </ol>
    </nav>
);
