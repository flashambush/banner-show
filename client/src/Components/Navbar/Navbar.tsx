import React from 'react';
import { NavLink } from 'react-router-dom';
import './navbar.styl';

export const Navbar = () => {

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container-xxl">
                <NavLink className="navbar-brand" to="/">banner.show</NavLink>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                        aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"/>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav me-auto">
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/campaign/list">Campaigns list</NavLink>
                        </li>
                        <li className={'nav-item'}>
                            <NavLink className={'nav-link'} to={'/page/template'}>Get Adobe Animate Template</NavLink>
                        </li>
                    </ul>
                    <NavLink className="nav-link btn btn-outline-primary" to="/banner/create">Upload new
                        mediaplan</NavLink>
                </div>
            </div>
        </nav>
    );
};