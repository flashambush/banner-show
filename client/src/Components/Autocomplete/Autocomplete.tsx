import React, { FC, Fragment, HTMLProps, useCallback, useEffect, useState } from 'react';
import Autosuggest from 'react-autosuggest';
import cn from 'classnames';
import './autocomplete.styl';
import { Field, FormikHelpers, useFormikContext } from 'formik';
import { BannerCreateForm } from '../../pages/MediaPlan/types';

export const Autocomplete: FC<Props<any>> = <T extends Record<string, string>>({ list, className, name, placeholder, keyName}: Props<T>) => {

    const [suggestions, setSuggestions] = useState<Array<T>>([]);
    const [inputValue, setInputValue] = useState<string>('');
    const { values, setFieldValue } = useFormikContext<BannerCreateForm>();

    const getSuggestionValue = (suggestion: T) => suggestion[keyName];

    const renderSuggestion = (suggestion: T) => (
        <div className={'autocomplete__suggestion'}>
            {suggestion[keyName]}
        </div>
    );

    const renderInputComponent = (inputProps: HTMLProps<HTMLInputElement>) => (
        <Fragment>
            <Field type="text"
                   className="form-control autocomplete__input"
                   id={name}
                   name={name}
                   placeholder={placeholder}
                   {...inputProps}/>
            <label htmlFor={name} className="form-label">{placeholder}</label>
        </Fragment>
    );

    const getSuggestions = useCallback((value: string) => {
        const inputValue = value.trim().toLowerCase();
        const inputLength = inputValue.length;
        return inputLength === 0 ? [] : list.filter((col: T) => col[keyName].toLowerCase().includes(inputValue));
    }, [list, keyName]);

    const onChange = useCallback((event, { newValue }) => {
        setInputValue(newValue);
        setFieldValue(name, newValue);
    }, [name, setFieldValue]);

    const onSuggestionsFetchRequested = useCallback(({ value }) => {
        setSuggestions(getSuggestions(value));
    }, [getSuggestions]);

    const onSuggestionsClearRequested = useCallback(() => {
        setSuggestions([]);
    }, []);

    const inputProps = {
        value: inputValue,
        onChange: onChange
    };

    useEffect(() => {
        values[name] == '' ? setInputValue('') : null;
    }, [values[name]]);

    return (
        <Autosuggest
            theme={{
                container: cn(className, 'autocomplete'),
                suggestionsContainerOpen: 'autocomplete__suggestions',
                suggestion: 'autocomplete__suggestion',
                suggestionsList: 'autocomplete__list'
            }}
            suggestions={suggestions}
            onSuggestionsFetchRequested={onSuggestionsFetchRequested}
            onSuggestionsClearRequested={onSuggestionsClearRequested}
            getSuggestionValue={getSuggestionValue}
            renderSuggestion={renderSuggestion}
            renderInputComponent={renderInputComponent}
            inputProps={inputProps}
        />
    );
};

type Props<T extends Record<string, string>> = {
    name: keyof BannerCreateForm;
    keyName: keyof T;
    placeholder: string;
    className: string;
    list: Array<T>;
}