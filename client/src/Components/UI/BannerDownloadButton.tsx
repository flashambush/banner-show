import React, { FC, Fragment, useCallback, useEffect, useState } from 'react';
import { downloadResource, getPublicDownloadLink } from '../../utils/getURLs';
import { Banner, ButtonProps as ButtonType, ButtonTypes } from '../../types';
import { BannerStatus } from '../../../../types/types';
import { Button } from './Button';

export const BannerDownloadButton: FC<Props> = ({
                                                    banner,
                                                    site = '',
                                                    ...props
                                                }) => {

    const [bs, setBs] = useState(banner.bannerStatus);

    const onDownloadClick = useCallback(() => {
        const prefix = site !== '' ? site : banner.platform;
        const url = getPublicDownloadLink(banner.banner_id);
        const filename = `${prefix}_${banner.width}x${banner.height}.zip`;
        downloadResource(url, filename);
    }, [site, banner.banner_id, banner.platform, banner.width, banner.height]);

    useEffect(() => {
        setBs(banner.bannerStatus);
    }, [banner, banner.bannerStatus]);

    return (
        <Fragment>
            {bs === BannerStatus.Approved ?
                <Button {...props}
                        variant={ButtonTypes.Primary}
                        onClick={onDownloadClick}>
                    Download
                </Button>
                :
                <div className="md input-group-text">in Development</div>
            }
        </Fragment>
    );
};

type Props = {
    banner: Banner,
    site?:string
} & ButtonType;