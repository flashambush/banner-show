import React, { FC } from 'react';
import { ButtonProps } from '../../types';
import cn from 'classnames';


export const Button: FC<ButtonProps> = ({
                                            variant,
                                            children,
                                            onClick,
                                            isOutlined = false,
                                            ...props
                                        }) => (
    <button {...props}
            className={cn(`btn btn-${isOutlined ? `outline-${variant}` : variant}`, props.className)}
            onClick={onClick}>
        {children}
    </button>
);

