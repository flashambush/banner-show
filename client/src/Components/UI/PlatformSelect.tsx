import React, { FC, useCallback } from 'react';


export const PlatformSelect: FC<Props> = ({ platformsList, platform, selectPlatform }) => {

    const onPlatformChange = useCallback((e) => {
        selectPlatform(e.target.value);
    }, [selectPlatform]);

    return (
        <select defaultValue={platform}
                value={platform}
                className="form-select"
                onChange={onPlatformChange}
                aria-label="Select platform">
            {platformsList.map((item) =>
                <option value={item} key={item}>{item}</option>
            )}
        </select>
    );
};

type Props = {
    platformsList: Array<string>,
    platform: string,
    selectPlatform: any
}