import { CopyToClipboard } from 'react-copy-to-clipboard';
import React, { FC, Fragment, useCallback, useEffect, useRef, useState } from 'react';
import { getPublicDownloadLink } from '../../utils/getURLs';
import ReactTooltip from 'react-tooltip';

export const CopyDownloadPublicLink: FC<{ bannerId: string }> = ({ bannerId }) => {

    const [downloadLink, setDownloadLink] = useState<string>('');
    const btnRef = useRef<HTMLButtonElement>(null);

    const onCopy = useCallback((str) => {
        if (!btnRef.current) {
            return void 0;
        }
        ReactTooltip.show(btnRef.current);
    }, [btnRef]);

    useEffect(() => {
        if (!bannerId) {
            return void 0;
        }
        setDownloadLink(getPublicDownloadLink(bannerId));
    }, [bannerId]);

    return (
        <Fragment>
            <CopyToClipboard
                onCopy={onCopy}
                text={downloadLink}>
                <button ref={btnRef}
                        className={'btn btn-outline-secondary copy'}
                        data-tip={`Link copied`}
                        data-for={'link-copied'}
                >
                    {'Copy download link'}
                </button>
            </CopyToClipboard>
        </Fragment>
    );
};