import React, { FC, PropsWithChildren } from 'react';

export const PublicHeader:FC<PropsWithChildren<{ title: string }>> = ({ title, children}) => (
    <header className={'bg-light'}>
        <div className="center">
            <div className={'row'}>
                <div className={'col-auto'}>
                    <h1>{title}</h1>
                </div>
                {children}
            </div>
        </div>
    </header>
);