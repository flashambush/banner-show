import React, { ChangeEventHandler, FC, useCallback, useState } from 'react';
import { getPublicPlatformPreviewLink } from '../../utils/getURLs';
import { pipe, prop } from 'ramda';

export const PublicPlatformLinkGroup: FC<{ campaignId: string, platform: string }> = ({ campaignId, platform }) => {

    const [site, setSite] = useState<string>(platform);

    const onSiteChange: ChangeEventHandler<HTMLInputElement> = useCallback(pipe(
        prop('currentTarget'),
        prop('value'),
        setSite
    ), []);

    return (
        <div
            className={'input-group input-group-sm'}>
            <span className="input-group-text">Platform name</span>
            <input className="form-control" defaultValue={platform} onChange={onSiteChange} type={'text'}/>
            <button disabled={true} className={'btn btn-outline-secondary'}>Copy link</button>
            <a href={`${getPublicPlatformPreviewLink(campaignId, platform)}/site/${site}`}
               className={'btn btn-outline-primary'}
               target={'_blank'}
               rel={'noreferrer'}>Preview</a>
        </div>
    );
};