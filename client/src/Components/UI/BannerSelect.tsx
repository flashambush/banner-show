import React, { FC, useCallback } from 'react';
import { Banner } from '../../types';
import { Func } from '../../../../types/types';


export const BannerSelect: FC<Props> = ({ banners, bannerId, selectBanner, isPublic = false}) => {

    const onBannerChange = useCallback((e) => {
        selectBanner(e.target.value);
    }, [selectBanner]);

    return (
        <select value={bannerId}
                className="form-select d-inline-block w-auto"
                onChange={onBannerChange}
                aria-label="Select banner format">
            {banners.map((item) =>
                <option value={item.banner_id}
                        key={item.banner_id}>{`${item.width}x${item.height} ${isPublic ? item.platform : ` - ${item.bannerStatus}`}`}</option>
            )
            }
        </select>
    );
};

type Props = {
    banners: Array<Banner>,
    bannerId: string,
    selectBanner: Func<[string], void>,
    isPublic?: boolean;
}