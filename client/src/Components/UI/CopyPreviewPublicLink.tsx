import { CopyToClipboard } from 'react-copy-to-clipboard';
import React, { FC, Fragment, useCallback, useEffect, useRef, useState } from 'react';
import { getPublicPreviewLink } from '../../utils/getURLs';
import ReactTooltip from 'react-tooltip';

export const CopyPreviewPublicLink: FC<{ bannerId: string, campaignId: string }> = ({ bannerId, campaignId }) => {

    const [previewLink, setPreviewLink] = useState<string>('');
    const btnRef = useRef<HTMLButtonElement>(null);

    const onCopy = useCallback((str) => {
        if (!btnRef.current) {
            return void 0;
        }
        ReactTooltip.show(btnRef.current);
    }, [btnRef]);

    useEffect(() => {
        if (!bannerId || !campaignId) {
            return void 0;
        }
        setPreviewLink(getPublicPreviewLink(bannerId, campaignId));
    }, [bannerId, campaignId]);

    return (
        <Fragment>
            <CopyToClipboard
                onCopy={onCopy}
                text={previewLink}>
                <button ref={btnRef}
                        className={'btn btn-outline-secondary copy'}
                        data-tip={`Link copied`}
                        data-for={'link-copied'}
                >
                    {'Copy preview link'}
                </button>
            </CopyToClipboard>
        </Fragment>
    );
};