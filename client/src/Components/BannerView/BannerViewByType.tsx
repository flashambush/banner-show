import React, { FC, Fragment } from 'react';
import { Banner } from '../../types';
import { SERVER_URL } from '../../constants';
import { BannerType } from '../../../../types/types';

export const BannerViewByType: FC<Banner> = (banner) => (
    <Fragment>
        {banner.type === BannerType.Tgb ?
            <img src={`${SERVER_URL}/banners/view/${banner.banner_id}.jpg`}
                 className={'d-flex mx-auto'}
                 width={banner.width}
                 height={banner.height}
                 alt={banner.platform}
                 style={{
                     width: `${banner.width}px`,
                     height: `${banner.height}px`,
                 }}/>
            : null
        }
        {banner.type === BannerType.Video ?
            <video src={`${SERVER_URL}/banners/view/${banner.banner_id}.mp4`}
                   className={'d-flex mx-auto'}
                   autoPlay={true}
                   loop={true}
                   width={banner.width}
                   height={banner.height}
                   style={{
                       width: `${banner.width}px`,
                       height: `${banner.height}px;`
                   }}/>
            : null
        }
        {banner.type === BannerType.Html ?
            <iframe frameBorder="0"
                    className={'d-flex mx-auto'}
                    title={`${banner.width}x${banner.height}`}
                    src={`${SERVER_URL}/banners/view/${banner.banner_id}/index.html`}
                    width={banner.width}
                    height={banner.height}
                    style={{
                        width: banner.width + (banner.width == 100 ? '%' : 'px'),
                        height: `${banner.height}px`
                    }}/>
            : null
        }
    </Fragment>
);