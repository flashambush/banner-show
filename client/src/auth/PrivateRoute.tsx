import React from 'react';
import { useAuth } from './useAuth';
import { Redirect, Route } from 'react-router-dom';
import { RouteProps } from 'react-router';
import { FC } from 'react';

export const PrivateRoute: FC<RouteProps & { defaultUrl: string }> = ({ defaultUrl, ...rest }) => {
    const auth = useAuth();

    return (
        auth.user
            ? <Route{...rest}/>
            : <Redirect to={{ pathname: defaultUrl }}/>
    );
};
