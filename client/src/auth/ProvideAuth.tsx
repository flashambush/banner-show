import React from 'react';
import { FC } from 'react';
import { authContext } from './authContext';
import { useProvideAuth } from './useAuthProvide';

export const ProvideAuth: FC<{}> = ({ children }) => {
    const auth = useProvideAuth();

    return (
        <authContext.Provider value={auth}>
            {children}
        </authContext.Provider>
    );
};
