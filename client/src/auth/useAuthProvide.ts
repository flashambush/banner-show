import { useState } from 'react';
import { useLocalStorage } from '../hooks/useLocalStorage';

export type User = {
    name: string;
    email: string;
};

export const defaultAuthContext: AuthContextApi = {
    user: undefined,
    token: undefined,
    signIn(data: { token: string; user: User }) {
        console.info(`Default context login!`);
        this.user = data.user;
        this.token = data.token;
    },
    signOut() {
        this.token = undefined;
        this.user = undefined;
    }
};

export type AuthData = {
    user: User;
    token: string;
}

type AuthContextApi = Partial<AuthData> & {
    signIn: (data: AuthData) => void;
    signOut: () => void;
};

export const useProvideAuth = () => {
    const [user, setUser] = useState<undefined | User>(undefined);
    const [token, setToken] = useLocalStorage('token', '');

    const signIn = (data: AuthData) => {
        console.info(`New login with data: ${JSON.stringify(data.user, null, 4)}`);
        setUser(data.user);
        setToken(data.token);
    };

    const signOut = () => {
        setUser(undefined);
        setToken('');
    };

    return {
        token,
        user,
        signIn,
        signOut
    };
};
