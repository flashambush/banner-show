import { BannerStatus, BannerType, Func } from '../../types/types';
import { HTMLProps } from 'react';

export type Banner = {
    hash: string
    banner_id: string; // format+platform
    platform: string;
    mediaPlanId: number;
    details: Array<string>;
    bannerStatus: BannerStatus;
    version: number; // версия креатива
    createdAt: string; // дата создания
    width: number;
    height: number;
    type: BannerType;
}

export type MediaPlan = {
    id: number;
    client: string;
    brand: string;
    campaign: string;
    createdAt: string;
    author: number;
}

export type ButtonProps = Omit<HTMLProps<HTMLButtonElement>, 'type'> & {
    variant: ButtonTypes;
    isOutlined?: boolean;
}

export enum NotificationType {
    Text = 'TEXT',
    Progress = 'PROGRESS'
}

export enum ButtonTypes {
    Primary = 'primary',
    Secondary = 'secondary',
    Success = 'success',
    Danger = 'danger',
    Warning = 'warning',
    Info = 'info',
    Light = 'light',
    Dark = 'dark',
    Link = 'link',
}
