import React, { ChangeEvent, Fragment, useCallback, useEffect, useMemo, useState } from 'react';
import { makeRequest, makeUrl, parse, ParsedRequest } from '../../services/request/request';
import { useAuth } from '../../auth/useAuth';
import { Banner, ButtonTypes, MediaPlan } from '../../types';
import { Link, useHistory, useParams } from 'react-router-dom';
import { findIndex, pipe, prop, propEq, values } from 'ramda';
import { BannerViewByType } from '../../Components/BannerView/BannerViewByType';
import { formatCreatedDate } from '../../utils/formatTime';
import { Loading } from '../../Components/Loading/Loading';
import { getResponseContent } from '../../utils/getResponseContent';
import { ButtonLoading } from '../../Components/Loading/ButtonLoading';
import { Breadcrumbs } from '../../Components/Breadcrumbs/Breadcrumbs';
import { CopyPreviewPublicLink } from '../../Components/UI/CopyPreviewPublicLink';
import { CopyDownloadPublicLink } from '../../Components/UI/CopyDownloadPublicLink';
import ReactTooltip from 'react-tooltip';
import { BannerDownloadButton } from '../../Components/UI/BannerDownloadButton';
import { getBannerLink } from '../../utils/getURLs';
import { Button } from '../../Components/UI/Button';
import { BannerStatus } from '../../../../types/types';
import { emptyMediaPlan } from '../MediaPlan/emptyMediaPlan';
import Select, { SingleValue } from 'react-select';
import { FormikConfig, useFormik } from 'formik';
import { mixed, object } from 'yup';


export const BannerView = () => {

    const findBannerIndexById = (id: string, list: Array<Banner>) => findIndex(propEq('banner_id', id), list);

    const { token } = useAuth();
    const history = useHistory();
    const { campaignId, bannerId } = useParams<{ campaignId: string, bannerId: string }>();
    const [banner, setBanner] = useState<Banner>();
    const [platformBannerList, setPlatformBannerList] = useState<Array<Banner>>([]);
    const [campaign, setCampaign] = useState<MediaPlan>(emptyMediaPlan);
    const [isArrowDisabled, setIsArrowDisabled] = useState<Record<string, boolean>>({left:true, right:true});
    const bannerIndex = useMemo<number>(() => findBannerIndexById(bannerId, platformBannerList), [bannerId, platformBannerList]);

    const bannerApprove = useCallback(() => {
        fetch(makeRequest(makeUrl(`/api/banners/${bannerId}/approve`), { method: 'POST' }, token))
            .then<BannerResponse>(parse)
            .then(getResponseContent)
            .then(setBanner);
    }, [bannerId, token]);

    const bannerDelete = useCallback(() =>
            fetch(makeRequest(makeUrl(`/api/banners/${bannerId}`), { method: 'DELETE' }, token))
                .then<BannerResponse>(parse)
                .then(getResponseContent)
                .then(pipe(prop('bannerStatus'))),
        [bannerId, token]);

    const selectBanner = useCallback((selectedBanner:SingleValue<Banner>) => {
        if (!selectedBanner) {
            return void 0;
        }
        history.push(getBannerLink(selectedBanner.banner_id, campaignId));
    }, [platformBannerList]);

    useEffect(() => {
        if (bannerIndex === 0) {
            setIsArrowDisabled({ left: true, right: false });
        } else if (bannerIndex === platformBannerList.length - 1) {
            setIsArrowDisabled({ left: false, right: true });
        } else if (bannerIndex === -1) {
            setIsArrowDisabled({ left: true, right: true });
        } else {
            setIsArrowDisabled({ left: false, right: false });
        }
    }, [bannerIndex]);

    const prevBanner = useCallback(() => {
        if (bannerIndex > 0) {
            selectBanner(platformBannerList[bannerIndex - 1]);
        }
    }, [bannerId, platformBannerList]);

    const nextBanner = useCallback(() => {
        if (bannerIndex < platformBannerList.length - 1) {
            selectBanner(platformBannerList[bannerIndex + 1]);
        }
    }, [bannerId, platformBannerList]);

    const onSubmit: FormikConfig<BannerUpdateForm>['onSubmit'] = useCallback((values, actions) => {
        if (!values.bannerFile) {
            return void 0;
        }
        const file: File = values.bannerFile[0];
        const form = new FormData();
        form.append('isFuriousCompression', values.isFuriousCompression.toString());
        form.append('bannerFile', file, file.name);

        return fetch(makeRequest(makeUrl(`/api/banners/${bannerId}`), {
            method: 'POST',
            body: form
        }, token, true))
            .then<ParsedRequest<{ type: string; id: string }>>(parse)
            .then(console.log)
            .catch((error) => {
                console.log(error);
            });
    }, [token, history, bannerId]);

    const formik = useFormik<BannerUpdateForm>({
        initialValues: {
            bannerFile: [],
            isFuriousCompression: false
        },
        onSubmit: onSubmit,
        validationSchema: object().shape({
            bannerFile: mixed().required(),
        })
    });

    const onChangeFile = useCallback((event: ChangeEvent<HTMLInputElement>) => {
        const files = Array.from(event.currentTarget.files ?? []);
        formik.setFieldValue(event.currentTarget.name, files);
    }, []);

    useEffect(() => {
        history.push(getBannerLink(bannerId, campaignId));
    }, [bannerId]);

    useEffect(() => {
        fetch(makeUrl(`/api/public/mediaPlan/${campaignId}`))
            .then<MediaplanResponse>(parse)
            .then(getResponseContent)
            .then(setCampaign);
    }, [campaignId]);

    useEffect(() => {
        fetch(makeRequest(makeUrl(`/api/banners/${bannerId}`), {}, token))
            .then<BannerResponse>(parse)
            .then(getResponseContent)
            .then(setBanner);
    }, [token, bannerId]);


    useEffect(() => {
        if (!banner) {
            return void 0;
        }
        fetch(makeRequest(makeUrl(`/api/mediaPlan/${campaignId}/banners?platform=${banner.platform}`), {}, token))
            .then<MediaplanListResponse>(parse)
            .then(getResponseContent)
            .then(setPlatformBannerList);
    }, [token, campaignId, banner?.platform]);

    return (
        <Fragment>
            {campaign && banner && platformBannerList ?
                <Fragment>
                    <Breadcrumbs>
                        <span>{campaign.client}</span>
                        <span>{campaign.brand}</span>
                        <Link to={`/campaign/${campaignId}`}>{campaign.campaign}</Link>
                        <Link to={`/campaign/${campaignId}/platform/${banner.platform}`}>{banner.platform}</Link>
                        <Select options={platformBannerList}
                                defaultValue={banner}
                                getOptionLabel={(option:Banner) => `${option.width}x${option.height}`}
                                getOptionValue={(option:Banner) => option.banner_id}
                                className="banner-select"
                                onChange={selectBanner}
                                aria-label="Select banner format"/>
                    </Breadcrumbs>
                    <h1>{`${banner.platform} ${banner.width}x${banner.height} `}
                        <span
                            className={`badge bg-${banner?.bannerStatus === BannerStatus.Error ? 'danger' : banner?.bannerStatus === BannerStatus.Approved ? 'success' : 'warning'}`}>{banner?.bannerStatus}</span>
                    </h1>
                    <hr/>
                    {banner?.bannerStatus === BannerStatus.Error ?
                        <div className="alert alert-danger" role="alert">
                            {/*{*/}
                            {/*    banner.details.map(detail =>*/}
                            {/*        <span>{detail}</span>*/}
                            {/*    )*/}
                            {/*}*/}
                        </div>
                        : banner?.bannerStatus === BannerStatus.Uploaded ?
                            <div className="alert alert-warning alert-dismissible fade show" role="alert">
                                Check the creative and <strong>approve</strong> it!
                                <button type="button"
                                        className="btn-close"
                                        data-bs-dismiss="alert"
                                        aria-label="Close"/>
                            </div>
                            :
                            null
                    }
                    <div className={'row'}>
                        <div className={'col'}>
                            <BannerViewByType {...banner}/>
                        </div>
                    </div>
                    <div className={'row mt-2'}>
                        <div className={'col-auto mx-auto'}>
                            <div className={'input-group'}>
                                <Button variant={ButtonTypes.Secondary}
                                        isOutlined={true}
                                        disabled={isArrowDisabled.left}
                                        onClick={prevBanner}>{'<'}</Button>
                                {banner?.bannerStatus === BannerStatus.Uploaded ?
                                    <Fragment>
                                        <Button variant={ButtonTypes.Success}
                                                onClick={bannerApprove}>
                                            {'Approve'}
                                        </Button>
                                        <Button variant={ButtonTypes.Danger}
                                                onClick={bannerDelete}>
                                            {'Delete'}
                                        </Button>
                                    </Fragment>
                                    : <Fragment>
                                        <CopyPreviewPublicLink bannerId={bannerId} campaignId={campaignId}/>
                                        <ReactTooltip type={'success'}
                                                      id={'link-copied'}
                                                      event={'copy'}
                                                      effect={'solid'}
                                                      eventOff={'mousemove'}
                                                      delayHide={1000}/>
                                    </Fragment>
                                }
                                {banner?.bannerStatus === BannerStatus.Approved ?
                                    <Fragment>
                                        <CopyDownloadPublicLink bannerId={bannerId}/>
                                        <BannerDownloadButton banner={banner}
                                                              variant={ButtonTypes.Secondary}
                                                              isOutlined={true}/>
                                    </Fragment>
                                    : null
                                }
                                <Button variant={ButtonTypes.Secondary}
                                        isOutlined={true}
                                        disabled={isArrowDisabled.right}
                                        onClick={nextBanner}>{'>'}</Button>
                            </div>
                        </div>
                    </div>

                    <hr/>
                    <div className={'row mt-3'}>
                        <div className={'col-12 col-lg-6 col-md-8 mx-auto'}>
                            <div className="card">
                                <div className="card-header">
                                    Upload new version
                                </div>
                                <div className="card-body">
                                    <h5 className="card-title">Current version: <span>{banner.version}</span></h5>
                                    <div
                                        className={'card-text mb-3'}>Uploaded: <span>{formatCreatedDate(banner.createdAt)}</span>
                                    </div>
                                    <form onSubmit={formik.handleSubmit}>
                                        <input type="file"
                                               className="form-control mb-3"
                                               name={'bannerFile'}
                                               onChange={(e) => onChangeFile(e)}
                                               aria-describedby="inputGroupFileAddon04"
                                               aria-label="Upload"/>
                                        <div className={'row'}>
                                            <div className={'col'}>
                                                <div className="form-check form-switch">
                                                    <input type="checkbox"
                                                           className="form-check-input"
                                                           name={'isFuriousCompression'}
                                                           checked={formik.values.isFuriousCompression}
                                                           onChange={(e) => {
                                                               formik.setFieldValue(e.currentTarget.name, e.currentTarget.checked);
                                                           }
                                                           }
                                                           id="furiousCheckbox"/>
                                                    <label className="form-check-label"
                                                           htmlFor="furiousCheckbox">Furious
                                                        compression</label>
                                                </div>
                                            </div>
                                            <div className={'col-auto'}>
                                                <Button variant={ButtonTypes.Primary}
                                                        type={'submit'}>
                                                    <ButtonLoading isLoading={false} title={'Upload'}/>
                                                </Button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </Fragment>
                : <Loading/>
            }
        </Fragment>
    );
};

type MediaplanListResponse = ParsedRequest<{
    content: Array<Banner>
}>

type MediaplanResponse = ParsedRequest<{
    content: MediaPlan
}>

type BannerResponse = ParsedRequest<{
    content: Banner
}>

type BannerUpdateForm = {
    bannerFile: Array<File>,
    isFuriousCompression: boolean
}