import macossafari from '../../img/pview/macossafari.png';
import macosff from '../../img/pview/macosff.png';
import macoschrome from '../../img/pview/macoschrome.png';
import windowsff from '../../img/pview/windowsff.png';
import windowschrome from '../../img/pview/windowschrome.png';
import React from 'react';

export const PviewFooter = () => (
    <footer className="md">
        <div className="center">
            <div>Для просмотра в режиме эмулятора мобильного устройства нажмите:</div>
            <div className="icons">
                <div className="hint xl">
                    <div className="text">maс OS Safari</div>
                    <img src={macossafari} alt={'MAC OS Safari'}/>
                </div>
                <div className="hint lg">
                    <div className="text ">maс OS Firefox</div>
                    <img src={macosff} alt={'MAC OS FireFox'}/>
                </div>
                <div className="hint">
                    <div className="text">maс OS Chrome</div>
                    <img src={macoschrome} alt={'MAC OS Chrome'}/>
                </div>
                <div className="hint lg">
                    <div className="text ">Windows Firefox</div>
                    <img src={windowsff} alt={'Windows FireFox'}/>
                </div>
                <div className="hint">
                    <div className="text">Windows Chrome</div>
                    <img src={windowschrome} alt={'Windows Chrome'}/>
                </div>
            </div>
            <div className="footnote xl">*Если ничего не произошло, зайдите в меню «Safari» &gt; «Настройки»,
                нажмите «Дополнения» и установите флажок «Показывать меню “Разработка” в строке меню».
            </div>
        </div>
        <div className="bg"/>
    </footer>
);