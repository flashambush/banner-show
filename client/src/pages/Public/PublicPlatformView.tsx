import React, { Fragment, useEffect, useState } from 'react';
import { Banner, ButtonTypes, MediaPlan } from '../../types';
import './pview.styl';
import { BannerViewByType } from '../../Components/BannerView/BannerViewByType';
import { useParams } from 'react-router-dom';
import { makeUrl, parse, ParsedRequest } from '../../services/request/request';
import { Loading } from '../../Components/Loading/Loading';
import { getResponseContent } from '../../utils/getResponseContent';
import { getCampaign } from '../../utils/getCampaign';
import { formatCreatedDate } from '../../utils/formatTime';
import { emptyMediaPlan } from '../MediaPlan/emptyMediaPlan';
import { BannerDownloadButton } from '../../Components/UI/BannerDownloadButton';
import { PublicHeader } from '../../Components/UI/PublicHeader';
import { Button } from '../../Components/UI/Button';

export const PublicPlatformView = () => {

    const { campaignId, platform, site } = useParams<{ campaignId: string, platform: string, site: string }>();
    const [hasError, setError] = useState<string | null>(null);
    const [mediaPlan, setMediaPlan] = useState<Array<Banner>>([]);
    const [campaign, setCampaign] = useState<MediaPlan>(emptyMediaPlan);

    useEffect(() => {
        fetch(makeUrl(`/api/public/mediaPlan/${campaignId}/banners?platform=${platform}`))
            .then<MediaPlanResponse>(parse)
            .then(getResponseContent)
            .then(setMediaPlan)
            .catch((e) => {
                setError(e.data);
            });
    }, [campaignId, platform]);

    useEffect(() => {
        getCampaign(campaignId).then(setCampaign);
    }, [campaignId]);

    return (
        <div className={'pview'}>
            {
                !hasError ?
                    <Fragment>
                        <PublicHeader title={campaign.campaign}>
                            <span className="col-auto label">Platform: <strong>{platform}</strong></span>
                            <div className={'col-auto'}>
                                <Button variant={ButtonTypes.Primary}>Download all</Button>
                            </div>
                        </PublicHeader>
                        <section>
                            {
                                mediaPlan.map(banner =>
                                    <div className="center" key={banner.banner_id}>
                                        <div className="info md">
                                            <div>AD size: <span>{`${banner.width}x${banner.height}`}</span></div>
                                            <div>Version: <span>{banner.version}</span></div>
                                            <div>Updated: <span>{formatCreatedDate(banner.createdAt)}</span></div>
                                            <div>Archive: <BannerDownloadButton variant={ButtonTypes.Primary}
                                                                                className={'btn-sm ms-2'}
                                                                                banner={banner}
                                                                                site={site}/>
                                            </div>
                                        </div>
                                        <BannerViewByType {...banner}/>
                                        <hr/>
                                    </div>
                                )
                            }
                        </section>
                    </Fragment>
                    : <Loading error={hasError}/>
            }
        </div>
    );
};

type MediaPlanResponse = ParsedRequest<{
    content: Array<Banner>
}>;

