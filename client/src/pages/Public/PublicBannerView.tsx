import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { Banner, ButtonTypes, MediaPlan } from '../../types';
import './pview.styl';
import { BannerViewByType } from '../../Components/BannerView/BannerViewByType';
import { useHistory, useParams } from 'react-router-dom';
import { makeUrl, parse, ParsedRequest } from '../../services/request/request';
import { append, groupBy, identity, ifElse, isNil, pipe, prop } from 'ramda';
import { Loading } from '../../Components/Loading/Loading';
import { getResponseContent } from '../../utils/getResponseContent';
import { getCampaign } from '../../utils/getCampaign';
import { formatCreatedDate } from '../../utils/formatTime';
import { PviewFooter } from './PviewFooter';
import { emptyMediaPlan } from '../MediaPlan/emptyMediaPlan';
import { BannerDownloadButton } from '../../Components/UI/BannerDownloadButton';
import { PublicHeader } from '../../Components/UI/PublicHeader';
import Select, { ActionMeta, SingleValue } from 'react-select';
import { getPublicPreviewLink } from '../../utils/getURLs';
import { optionsGroupedByPlatform } from '../../utils/reactSelectOptions';

export const PublicBannerView = () => {

    const { campaignId, bannerId, site } = useParams<{ campaignId: string, bannerId: string, site: string }>();
    const [banner, setBanner] = useState<Banner | undefined>();
    const [error, setError] = useState<string | undefined>();
    const [mediaPlan, setMediaPlan] = useState<Array<Banner>>([]);
    const [campaign, setCampaign] = useState<MediaPlan>(emptyMediaPlan);
    const history = useHistory();

    useEffect(() => {
        fetch(makeUrl(`/api/public/mediaPlan/${campaignId}/banners`))
            .then<MediaPlanResponse>(parse)
            .then(getResponseContent)
            .then(setMediaPlan);
    }, [campaignId]);

    useEffect(() => {
        getCampaign(campaignId).then(setCampaign);
    }, [campaignId]);

    useEffect(() => {
        fetch(makeUrl(`/api/public/banners/${bannerId}`))
            .then<BannerResponse>(parse)
            .then(getResponseContent)
            .then(
                ifElse(
                    isNil,
                    identity,
                    setBanner
                )
            ).catch((e) => {
            setError(e.data.message);
        });
    }, [campaignId, bannerId]);

    const selectBanner = useCallback((selectedBanner:SingleValue<Banner>, action: ActionMeta<Banner>) => {
        if (!selectedBanner) {
            return void 0;
        }
        history.push(getPublicPreviewLink(selectedBanner.banner_id, campaignId, false));
    }, [campaignId]);

    return (
        <div className={'pview'}>
            {
                banner ?
                    <Fragment>
                        <PublicHeader title={campaign.campaign}>
                            <span className="col-auto">Формат:</span>
                            <div className={'col-auto'}>
                                <Select options={optionsGroupedByPlatform(mediaPlan)}
                                        defaultValue={banner}
                                        getOptionLabel={(option:Banner) => `${option.width}x${option.height}`}
                                        getOptionValue={(option:Banner) => option.banner_id}
                                        className="banner-select"
                                        onChange={selectBanner}
                                        aria-label="Select banner format"/>
                            </div>
                            <div className={'col-auto'}>
                                <BannerDownloadButton
                                    variant={ButtonTypes.Primary}
                                    banner={banner}
                                    site={site}
                                />
                            </div>
                        </PublicHeader>
                        <section>
                            <div className="center">
                                <div className="info md">
                                    <div>Площадка: <span>{banner.platform}</span></div>
                                    <div>Итерация: <span>{banner.version}</span></div>
                                    <div>Обновлен: <span>{formatCreatedDate(banner.createdAt)}</span></div>
                                </div>
                                <BannerViewByType {...banner}/>
                            </div>
                        </section>
                    </Fragment>
                    : <Loading error={error}/>
            }
            <PviewFooter/>
        </div>
    );
};




type BannerResponse = ParsedRequest<{
    content: Banner
}>

type MediaPlanResponse = ParsedRequest<{
    content: Array<Banner>
}>;

