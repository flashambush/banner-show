import React, { ChangeEventHandler, FC, useCallback, useState } from 'react';
import { useAuth } from '../../auth/useAuth';
import { pipe, prop } from 'ramda';
import { useHistory } from 'react-router-dom';
import { getToken } from '../../services/request/getToken';

export const Login: FC = () => {
    const auth = useAuth();
    const history = useHistory();
    const [username, setUserName] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [hasError, setHasError] = useState<boolean>(false);

    const makeChangeHandler = (set: (value: string) => void): ChangeEventHandler<HTMLInputElement> =>
        pipe(
            prop('target'),
            prop('value'),
            set
        );

    // eslint-disable-next-line
    const onChangeUserName = useCallback(makeChangeHandler(setUserName), []);
    // eslint-disable-next-line
    const onChangePassword = useCallback(makeChangeHandler(setPassword), []);

    const onSubmit = useCallback((e) => {
        e.preventDefault();

        setHasError(false);

        return getToken(username, password)
            .then(prop('data'))
            .then(auth.signIn)
            .then(() => {
                history.push('/');
            })
            .catch((e) => {
                console.error(e);
                setHasError(true);
            });
    }, [username, password, auth, history]);

    return (
        <div className={'row h-100 align-items-center justify-content-center'}>
            <div className="col-4">
                <form onSubmit={onSubmit}>
                    <div className="form-floating mb-3">
                        <input type="text"
                               onChange={onChangeUserName}
                               className="form-control"
                               id="username"
                               placeholder={'username'}
                               name={'username'}/>
                        <label htmlFor="username" className="form-label">User name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input type="password"
                               onChange={onChangePassword}
                               name={'password'}
                               className="form-control"
                               placeholder={'your secret password'}
                               id="password"/>
                        <label htmlFor="password" className="form-label">Password</label>
                    </div>
                    <div className="mb-3 form-check">
                        <input type="checkbox" className="form-check-input" id="check"/>
                        <label className="form-check-label" htmlFor="check">Check me out</label>
                    </div>
                    {hasError ? <div style={{ color: 'red' }}>Wrong password or user name!</div> : null}
                    <button type="submit" className="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    );
};