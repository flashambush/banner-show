import { BannerType } from '../../../../types/types';
import { ParsedRequest } from '../../services/request/request';

export type MediaPlanCreateCallback = ParsedRequest<{
    id: string;
    type: string;
}>

export type PlatformItem = {
    platformName: string;
}

export type BrandItem = {
    brandName: string;
};

export type ClientItem = {
    clientName: string;
}

export type Dictionaries = {
    platforms: Array<PlatformItem>;
    brands: Array<BrandItem>;
    clients: Array<ClientItem>;
}

export type BannerCreateForm = {
    client: string;
    campaign: string;
    brand: string;
    banners: Array<BannerCreateFormItem>;
}

export type BannerCreateFormItem = {
    platform: string;
    type:BannerType;
    files: Array<File>;
};