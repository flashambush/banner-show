import React, { Fragment, useEffect, useState } from 'react';
import { makeRequest, makeUrl, parse, ParsedRequest } from '../../services/request/request';
import { useAuth } from '../../auth/useAuth';
import { useParams } from 'react-router-dom';
import { Banner, MediaPlan } from '../../types';
import { groupBy, pipe, prop, toPairs } from 'ramda';
import { formatCreatedDate } from '../../utils/formatTime';
import ReactTooltip from 'react-tooltip';
import { Loading } from '../../Components/Loading/Loading';
import { getResponseContent } from '../../utils/getResponseContent';
import { getCampaign } from '../../utils/getCampaign';
import { BannerLsit } from '../../Components/BannerList/BannerList';
import './mediaPlanView.styl';
import { PublicPlatformLinkGroup } from '../../Components/UI/PublicPlatformLinkGroup';
import { Breadcrumbs } from '../../Components/Breadcrumbs/Breadcrumbs';

export const MediaPlanView = () => {
    const { token } = useAuth();
    const { campaignId, platform } = useParams<{ campaignId: string, platform: string }>();
    const [platforms, setPlatforms] = useState<Array<[string, Array<Banner>]>>([]);
    const [campaign, setCampaign] = useState<MediaPlan>({
        client: '',
        brand: '',
        campaign: '',
        createdAt: '',
        id: 0,
        author: 0
    });
    const [isLoaded, setLoading] = useState(true);

    useEffect(() => {
        getCampaign(campaignId).then(setCampaign);
    }, [campaignId]);

    useEffect(() => {
        //fetch(makeRequest(makeUrl(platform ? `/api/mediaPlan/${campaignId}/banners?platform=${platform}` : `/api/mediaPlan/${campaignId}/banners`), {}, token))
        fetch(makeRequest(makeUrl(`/api/mediaPlan/${campaignId}/banners`), {}, token))
            .then<MediaplanListResponse>(parse)
            .then(
                pipe<[MediaplanListResponse], Array<Banner>, void>(
                    getResponseContent,
                    pipe(
                        pipe<[Array<Banner>], Record<string, Array<Banner>>, Array<[string, Array<Banner>]>>(
                            groupBy<Banner>(prop('platform')),
                            toPairs
                        ),
                        setPlatforms
                    )
                )
            ).then(() => {
            setLoading(true);
        });
    }, [token, platform, campaignId]);

    useEffect(() => {
        ReactTooltip.rebuild();
    }, [platforms]);

    return (
        <Fragment>
            {
                isLoaded ?
                    <Fragment>
                        <Breadcrumbs>
                            <span>{campaign.brand}</span>
                            <span>{campaign.client}</span>
                        </Breadcrumbs>
                        <h1>{campaign.campaign}</h1>
                        <div className={'row'}>
                            <div className={'col-auto'}><span
                                className={'text-muted'}>CreatedAt: </span>{formatCreatedDate(campaign.createdAt)}</div>
                        </div>

                        <hr/>
                        <div className={'row mediaplanViewList'}>
                            {
                                platforms.map(([platform, bannerList]) =>
                                    <div className={'col-12 mediaplanViewList__platform'} key={platform}>
                                        <div className={'d-flex flex-row'}>
                                            <h2 className={'py-2'}>{platform}</h2>
                                            <div className={'col-auto mediaplanViewList__platform__controls'}>
                                                <PublicPlatformLinkGroup campaignId={campaignId} platform={platform}/>
                                            </div>
                                        </div>
                                        <BannerLsit bannerList={bannerList} campaignId={campaignId}/>
                                    </div>
                                )
                            }
                        </div>
                    </Fragment>
                    :
                    <Loading/>
            }
        </Fragment>
    );
};

type MediaplanListResponse = ParsedRequest<{
    content: Array<Banner>
}>
