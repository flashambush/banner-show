import React, { ChangeEvent, Fragment, MouseEvent, useCallback, useEffect, useRef, useState } from 'react';
import { useAuth } from '../../auth/useAuth';
import { createMediaPlan, loadDictionaries } from './helpers';
import {
    BannerCreateForm,
    BannerCreateFormItem,
    BrandItem,
    ClientItem,
    MediaPlanCreateCallback,
    PlatformItem
} from './types';
import { ErrorMessage, Field, FieldArray, Form, Formik, FormikConfig, FormikErrors, FormikHelpers } from 'formik';
import { array, object, string } from 'yup';
import { Autocomplete } from '../../Components/Autocomplete/Autocomplete';
import { path } from 'ramda';
import { useHistory } from 'react-router-dom';
import { parse } from '../../services/request/request';
import { ButtonLoading } from '../../Components/Loading/ButtonLoading';
import { socketManager } from '../../services/socket';
import { BannerType } from '../../../../types/types';
import { bannerProgressDone, bannerProgressUpdated } from '../../models/notification';

const requiredMessage = 'Field is required!';
const validationSchema = object().shape({
    client: string().required(requiredMessage),
    campaign: string().required(requiredMessage),
    brand: string().required(requiredMessage),
    banners: array().required().min(1).of(
        object().shape({
            platform: string().required(requiredMessage),
            files: array()
                .required(requiredMessage)
                .min(1, 'Must have at least 1 file!')
        })
    )
});

export const MediaPlanCreate = () => {

    const { token } = useAuth();
    const history = useHistory();


    const bannerItem: BannerCreateFormItem = {
        platform: '',
        type: BannerType.Html,
        files: []
    };

    const initialValues: BannerCreateForm = {
        client: '',
        campaign: '',
        brand: '',
        banners: [
            bannerItem
        ]
    };

    const [platformsList, setPlatformsList] = useState<Array<PlatformItem>>([]);
    const [clientsList, setClientsList] = useState<Array<ClientItem>>([]);
    const [brandsList, setBrandsList] = useState<Array<BrandItem>>([]);
    const [isMediaPlanUploading, setMediaPlanUploading] = useState<boolean>(false);
    const [error, hasError] = useState<string | null>(null);
    const fileInputRef = useRef<HTMLInputElement>(null);

    const onSubmit: FormikConfig<BannerCreateForm>['onSubmit'] = useCallback((values, actions) => {
        setMediaPlanUploading(true);
        return createMediaPlan(values, token as string)
            .then<MediaPlanCreateCallback>(parse)
            .then(() => {
                setMediaPlanUploading(false);
                actions.resetForm();
            })
            .catch((error) => {
                console.log(error);
                hasError(error.message);
                setMediaPlanUploading(false);
            });
    }, [token, history]);

    const onReset = useCallback(() => {
        if (!fileInputRef.current) {
            return void 0;
        }
        fileInputRef.current.value = '';
    }, [fileInputRef]);

    const onChangeFile = useCallback((event: ChangeEvent<HTMLInputElement>, setFieldValue: FormikHelpers<BannerCreateForm>['setFieldValue']) => {
        const files = Array.from(event.currentTarget.files ?? []);
        setFieldValue(event.currentTarget.name, files);
    }, []);

    useEffect(() => {
        socketManager.subscribe('bannersPrepareDone', bannerProgressDone);
        socketManager.subscribe('bannersPrepareProgress', bannerProgressUpdated);
    }, []);

    useEffect(() => {
        loadDictionaries()
            .then(({ clients, brands, platforms }) => {
                setBrandsList(brands);
                setClientsList(clients);
                setPlatformsList(platforms);
            });
    }, []);

    const isValid = useCallback((errors: FormikErrors<BannerCreateForm>, name: Array<string | number>) =>
            path(name, errors) ? 'is-invalid' : ''
        , []);

    return (
        <Fragment>
            <h1>Create new AD campaign</h1>
            {error ?
                <div>{error}</div>
                : null
            }
            <div className={'row'}>
                <div className={'col'}>
                    <Formik initialValues={initialValues}
                            enableReinitialize={true}
                            validationSchema={validationSchema}
                            validateOnChange={true}
                            validateOnBlur={false}
                            onReset={onReset}
                            onSubmit={onSubmit}>
                        {({ values, setFieldValue, errors }) => (
                            <Form>
                                <div className={'row mb-3'}>
                                    <div className={'col'}>
                                        <Autocomplete className={`form-floating ${isValid(errors, ['client'])}`}
                                                      list={clientsList}
                                                      keyName={'clientName'}
                                                      name={'client'}
                                                      placeholder={'Client'}/>
                                        <ErrorMessage name={'client'} component="div" className="invalid-feedback"/>
                                    </div>
                                    <div className={'col'}>
                                        <Autocomplete className={`form-floating ${isValid(errors, ['brand'])}`}
                                                      list={brandsList}
                                                      keyName={'brandName'}
                                                      name={'brand'}
                                                      placeholder={'Brand'}/>
                                        <ErrorMessage name={'brand'} component="div" className="invalid-feedback"/>
                                    </div>
                                    <div className={'col'}>
                                        <div className="form-floating">
                                            <Field type="text"
                                                   className={`form-control ${isValid(errors, ['campaign'])}`}
                                                   id="campaign"
                                                   name={'campaign'}
                                                   placeholder="Campaign"/>
                                            <label htmlFor="campaign" className="form-label">Campaign</label>
                                            <ErrorMessage name={'campaign'} component="div"
                                                          className="invalid-feedback"/>
                                        </div>
                                    </div>
                                </div>
                                <h2>Mediaplan</h2>
                                <FieldArray name={'banners'}>
                                    {(arrayHelpers) => (
                                        <Fragment>
                                            {
                                                values.banners.map((banner, i) => (
                                                    <div className={'row'} key={i}>
                                                        <div className={'col-6 col-xl-4'}>
                                                            <div className="mb-3 form-floating">
                                                                <Field
                                                                    className={`form-select ${isValid(errors, ['banners', i, 'platform'])}`}
                                                                    component={'select'}
                                                                    id={`banners_${i}_platform`}
                                                                    name={`banners.${i}.platform`}>
                                                                    <option disabled={true} defaultValue={'choose one'}
                                                                            value={''}>choose one
                                                                    </option>
                                                                    {platformsList.map((platform, index) => (
                                                                        <option key={index}
                                                                                value={platform.platformName}>{platform.platformName}</option>
                                                                    ))}
                                                                </Field>
                                                                <label htmlFor="platform">Selected platform</label>
                                                                <ErrorMessage name={`banners.${i}.platform`}
                                                                              component="div"
                                                                              className="invalid-feedback"/>
                                                            </div>
                                                        </div>
                                                        <div className={'col-6 col-xl-2'}>
                                                            <div className="mb-3 form-floating">
                                                                <Field
                                                                    className={`form-select ${isValid(errors, ['banners', i, 'type'])}`}
                                                                    component={'select'}
                                                                    id={`banners_${i}_type`}
                                                                    name={`banners.${i}.type`}>
                                                                    <option value={'HTML5'}>HTML5</option>
                                                                    <option value={'TGB'} disabled={true}>TGB</option>
                                                                    <option value={'VIDEO'} disabled={true}>Video
                                                                    </option>
                                                                </Field>
                                                                <label htmlFor="platform">Banner type</label>
                                                                <ErrorMessage name={`banners.${i}.platform`}
                                                                              component="div"
                                                                              className="invalid-feedback"/>
                                                            </div>
                                                        </div>
                                                        <div className="mb-3 form-group col">
                                                            <div className="input-group">
                                                                <input type="file"
                                                                       ref={fileInputRef}
                                                                       onChange={(e: ChangeEvent<HTMLInputElement>) => onChangeFile(e, setFieldValue)}
                                                                       accept={'application/zip'}
                                                                       className={`form-control ${isValid(errors, ['banners', i, 'files'])} lg`}
                                                                       id={`banners.${i}.files`}
                                                                       multiple={true}
                                                                       name={`banners.${i}.files`}
                                                                />
                                                                <label className="input-group-text d-none d-lg-flex"
                                                                       htmlFor="file">zip</label>
                                                                {i > 0 ?
                                                                    <button className="btn btn-outline-danger"
                                                                            data-i={i}
                                                                            type="button"
                                                                            onClick={(e: MouseEvent<HTMLButtonElement>) => {
                                                                                if (e.currentTarget.dataset.i) {
                                                                                    arrayHelpers.remove(Number.parseInt(e.currentTarget.dataset.i));
                                                                                } else {
                                                                                    return void 0;
                                                                                }
                                                                            }
                                                                            }>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="32"
                                                                             height="32"
                                                                             fill="currentColor" className="bi bi-x"
                                                                             viewBox="0 0 16 16">
                                                                            <path
                                                                                d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                                                                        </svg>
                                                                    </button>
                                                                    : null
                                                                }
                                                                <ErrorMessage name={`banners.${i}.files`}
                                                                              component="div"
                                                                              className="invalid-feedback"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                ))
                                            }
                                            <div className="btn-group mt-3" role="group">
                                                <button className={'btn btn-outline-secondary btn-lg'}
                                                        disabled={isMediaPlanUploading}
                                                        onClick={() => arrayHelpers.push(bannerItem)}>
                                                    Add more platform
                                                </button>
                                                <button className={'btn btn-primary btn-lg'}
                                                        disabled={isMediaPlanUploading}
                                                        type={'submit'}>
                                                    <ButtonLoading isLoading={isMediaPlanUploading}
                                                                   title={'Upload campaign'}/>
                                                </button>
                                            </div>
                                        </Fragment>
                                    )}
                                </FieldArray>
                            </Form>
                        )}
                    </Formik>
                </div>
            </div>
        </Fragment>
    );
};