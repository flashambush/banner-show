import React, { Fragment, useCallback, useEffect, useState } from 'react';
import { makeRequest, makeUrl, parse, ParsedRequest } from '../../services/request/request';
import { useAuth } from '../../auth/useAuth';
import { useHistory } from 'react-router-dom';
import { formatCreatedDate } from '../../utils/formatTime';
import { MediaPlan } from '../../types';
import { getResponseContent } from '../../utils/getResponseContent';
import { NoCampaigns } from '../../Components/NoData/NoCampaigns';
import { Loading } from '../../Components/Loading/Loading';

export const MediaPlansList = () => {
    const { token } = useAuth();
    const history = useHistory();
    const [mediaplans, setMediaPlans] = useState<Array<MediaPlan>>([]);
    const [isLoading, setLoading] = useState<boolean>(false);

    useEffect(() => {
        setLoading(true);
        fetch(makeRequest(makeUrl('/api/mediaPlan/list'), {}, token))
            .then<MediaplanListResponse>(parse)
            .then(getResponseContent)
            .then(setMediaPlans)
            .then(() => {
                setLoading(false);
            });
    }, [token]);

    const onCampaignClick = useCallback((e) => {
        history.push(`/campaign/${e.currentTarget.dataset.id}`);
    }, [history]);

    return (
        <Fragment>
            <h1>Campaigns</h1>
            <div className={'row'}>
                <div className={'col'}>
                    {
                        isLoading ?
                            <Loading/>
                            :
                            mediaplans.length > 0 ?
                                <table className="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Campaign</th>
                                        <th>Brand</th>
                                        <th>Client</th>
                                        <th className={'d-none d-md-table-cell'}>Created</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {mediaplans.map(item => (
                                        <tr onClick={onCampaignClick} key={item.id} data-id={item.id}>
                                            <td>{item.campaign}</td>
                                            <td>{item.brand}</td>
                                            <td>{item.client}</td>
                                            <td className={'d-none d-md-table-cell'}>{formatCreatedDate(item.createdAt)}</td>
                                        </tr>
                                    ))}
                                    </tbody>
                                </table>
                                :
                                <NoCampaigns/>
                    }
                </div>
            </div>
        </Fragment>
    );
};

type MediaplanListResponse = ParsedRequest<{
    content: Array<MediaPlan>
}>