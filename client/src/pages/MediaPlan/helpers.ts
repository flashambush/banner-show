import { BannerCreateForm, Dictionaries } from './types';
import { getBrandDictionary, getClientDictionary, getPlatformDictionary } from '../../services/dictioneries';
import { makeRequest, makeUrl } from '../../services/request/request';


export const loadDictionaries = () =>
    Promise
        .all([
            getPlatformDictionary(),
            getBrandDictionary(),
            getClientDictionary()
        ])
        .then(([platforms, brands, clients]) => ({
            platforms,
            brands,
            clients
        }) as Dictionaries);

export const createMediaPlan = (values: BannerCreateForm, token: string) => {
    const formData = new FormData();

    formData.set('client', values.client);
    formData.set('brand', values.brand);
    formData.set('campaign', values.campaign);

    values.banners.forEach((banner, index) => {
        formData.append(`banner.${index}.platform`, banner.platform);
        banner.files.forEach((file) => {
            formData.append(`banner.${index}.file`, file, file.name);
        });
    });

    return fetch(makeRequest(makeUrl('/api/mediaPlan/create'), {
        method: 'POST',
        body: formData
    }, token, true));
};