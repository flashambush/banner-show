import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

const DownloadTemplate = () => (
    <Fragment>
        <h1>Prepare your files</h1>
        <div className={'row'}>
            <div className={'col-12 col-lg-4'}>
                <div className="card">
                    <div className="card-header">
                        Step 1
                    </div>
                    <div className="card-body">
                        <h5 className="card-title">Скачать специальный шаблон</h5>
                        <p className="card-text">Шаблон для версии Adobe Animate CC 20+</p>
                        <a href={'https://www.dropbox.com/s/6o0fznxgz7po112/master.compiled.html?dl=0'}
                           download={true}
                           className={'btn btn-outline-primary'}>Download template</a>
                    </div>
                </div>
            </div>
            <div className={'col-12 col-lg-4'}>
                <div className="card">
                    <div className="card-header">
                        Step 2
                    </div>
                    <div className="card-body">
                        <h5 className="card-title">Опубликовать баннер</h5>
                        <div className="card-text">
                            <ul>
                                <li>Открыть баннер в Adobe Animate CC 20+</li>
                                <li>Импортировать шаблон</li>
                                <li>Опубликовать баннер</li>
                                <li>Повторить эти шаги для каждого ресайза</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div className={'col-12 col-lg-4'}>
                <div className="card">
                    <div className="card-header">
                        Step 3
                    </div>
                    <div className="card-body">
                        <h5 className="card-title">Загрузить</h5>
                        <p className="card-text">Загрузить ресайзы в систему и получить подготовленные по ТТ площадок
                            рекламные материалы</p>
                        <Link to="/banner/create" className="btn btn-primary">Upload your AD campaign</Link>
                    </div>
                </div>
            </div>
        </div>
    </Fragment>
);

export { DownloadTemplate as default };