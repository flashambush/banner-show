import { makeRequest, makeUrl } from './request';

export const getUserData = (token: string) =>
    fetch(makeRequest(makeUrl('/api/user'), {}, token));