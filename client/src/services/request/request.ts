import { curry, mergeDeepRight } from 'ramda';
import { SERVER_URL } from '../../constants';

const DEFAULT_HEADERS = {
    'Content-Type': 'application/json'
};

export const makeRequestOptions = curry((defaultData: RequestInit, options: RequestInit) =>
    mergeDeepRight(defaultData, options ?? {}));

export const makeRequest = (info: RequestInfo, options?: RequestInit, token?: string, skipContentType?: boolean): Request =>
    new Request(
        info,
        makeRequestOptions({
                headers: token
                    ? mergeDeepRight(skipContentType ? {} : DEFAULT_HEADERS, { Authorization: token })
                    : (skipContentType ? {} : DEFAULT_HEADERS)
            }, (options ?? {})
        )
    );

export const makeUrl = (path: string) => `${SERVER_URL}${path}`;

export const parse = <T>(r: Response): Promise<ParsedRequest<T>> => {
    const isJSON = (r.headers.get('Content-Type') ?? '').includes('application/json');

    const makeResponse = <T>(data: T) => ({
        url: r.url,
        status: r.status,
        response: r,
        data
    });

    const getStream = () => isJSON
        ? r.json().then(makeResponse)
        : r.text().then(makeResponse);

    return r.ok
        ? getStream()
        : getStream()
            .then((data) => Promise.reject(data));
};

export type ParsedRequest<T> = {
    url: string;
    status: number;
    response: Response;
    data: T
};
