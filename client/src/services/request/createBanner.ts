import { makeRequest, makeUrl, parse, ParsedRequest } from './request';
import { Moment } from 'moment';


export const createBanner = (form: FormData, token: string) =>
    fetch(makeRequest(makeUrl('/banner/create'), {
        method: 'POST',
        body: form,
    }, token, true))
        .then<ParsedRequest<Banner>>(parse);

export type Banner = {
    id: string;
    client: string;
    brand: string;
    campaign: string;
    createdAt: Moment;
    lastModify: Moment;
}
