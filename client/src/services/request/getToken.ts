import { makeRequest, makeUrl, parse, ParsedRequest } from './request';
import { AuthData } from '../../auth/useAuthProvide';

const makeOptions = (user: string, password: string) =>
    ({ method: 'POST', body: JSON.stringify({ username: user, password }) });

export const getToken = (user: string, password: string): Promise<ParsedRequest<AuthData>> =>
    fetch(makeRequest(makeUrl('/api/token'), makeOptions(user, password)))
        .then<ParsedRequest<AuthData>>(parse);
