import { loadGoogleSheets, string } from 'parse-googlesheets';

const GOOGLE_API_KEY = 'AIzaSyBcPECwiIN4TJH-l7ktcnJGHqd4fiVF7Aw';

const TT_SHEET_ID = '1aJ3Yr7g_l0QlsVY6-Xe5fMUHExMJmdTQlVdyo2tdBLk';
const TT_SCHEMA = {
    platformName: {
        parse: string,
        columnName: 'Площадка'
    }
};
const TT_RANGE = 'tt';

const CLIENTS_SHEET_ID = '1ok08Cm4wUNM-MBDN-tgJ74uB9hq9xxcEzoAX9EtY-Xw';
const CLIENTS_SCHEMA = {
    clientName: {
        parse: string,
        columnName: 'Клиент'
    }
};
const CLIENTS_RANGE = 'client';

const BRANDS_SHEET_ID = '1rh6iOAcKENLMIhW37wuBSd8ZtSBmV7fFnkOqUs0Sm4E';
const BRANDS_SCHEMA = {
    brandName: {
        parse: string,
        columnName: 'Бренд'
    }
};
const BRANDS_RANGE = 'brand';

export const getPlatformDictionary = () => {
    return loadGoogleSheets(TT_SCHEMA, TT_RANGE, TT_SHEET_ID, GOOGLE_API_KEY);
};

export const getClientDictionary = () => {
    return loadGoogleSheets(CLIENTS_SCHEMA, CLIENTS_RANGE, TT_SHEET_ID, GOOGLE_API_KEY);
};

export const getBrandDictionary = () => {
    return loadGoogleSheets(BRANDS_SCHEMA, BRANDS_RANGE, TT_SHEET_ID, GOOGLE_API_KEY);
};