import io from 'socket.io-client';
import { User } from '../auth/useAuthProvide';
import { SOCKET_URL } from '../constants';
import { Func } from '../../../types/types';
import { bannerProgressUpdated } from '../models/notification';

export const socket = io(SOCKET_URL, {
    transports: ['websocket']
}).connect();

class SocketManager<Map extends Record<string, RequestPair<any, any, any>>> {
    private static counter = 0;
    private activeRequest: Record<string, { resolve: Func<[any], any>, reject: Func<[any], any> }> = Object.create(null);

    constructor() {
        socket.on('response', (data: SocketResponse<string, any>) => {
            if (!data.requestId) {
                return void 0;
            }
            if (!this.activeRequest[data.requestId]) {
                console.error('Wrong response');
            }
            if (data.ok) {
                this.activeRequest[data.requestId].resolve(data);
            } else {
                this.activeRequest[data.requestId].reject(data);
            }
            delete this.activeRequest[data.requestId];
        });
    }

    private static makeId = () => `id-${SocketManager.counter++}`;

    public request<Url extends keyof Map>(url: Url, body: RequestBody<Map, Url>): Promise<ResponseType<Map, Url>> {
        return new Promise((resolve, reject) => {
            const requestId = SocketManager.makeId();

            this.activeRequest[requestId] = {
                resolve,
                reject
            };

            socket.emit('request', {
                url,
                body,
                requestId
            });
        });
    }

    // TODO remove any
    public subscribe(topic: string, handler: Func<[any], void>) {
        socket.on(topic, handler);
        socket.emit('subscribe', { topic });
    }

}

export const socketManager = new SocketManager<SocketRequestMap>();

type RequestBody<Map extends Record<string, RequestPair<any, any, any>>, Url extends keyof Map> =
    'request' extends keyof Map[Url] ? 'body' extends keyof Map[Url]['request'] ? Map[Url]['request']['body'] : never : never;

type ResponseType<Map extends Record<string, RequestPair<any, any, any>>, Url extends keyof Map> =
    'response' extends keyof Map[Url] ? Map[Url]['response'] : never;

type SocketRequest<RequestURL extends string, Body> = {
    url: RequestURL;
    body: Body;
    requestId: string;
}

type SocketResponseSuccess<RequestURL extends string, Body> = {
    ok: true;
    status: 200;
    url: RequestURL;
    body: Body;
    requestId: string;
}

type SocketResponseError<RequestURL extends string> = {
    ok: false;
    status: number;
    url: RequestURL;
    requestId: string;
    body: string;
}

type SocketResponse<RequestURL extends string, ResponseBody> =
    SocketResponseSuccess<RequestURL, ResponseBody>;

type RequestPair<RequestURL extends string, RequestBody, ResponseBody> = {
    request: SocketRequest<RequestURL, RequestBody>;
    response: SocketResponse<RequestURL, ResponseBody>;
}

type SocketRequestMap = {
    'auth': RequestPair<'auth', string, User>;
}
