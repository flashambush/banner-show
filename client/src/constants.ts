export const SERVER_URL = import.meta.env.VITE_SERVER_URL as string;
export const BASE_URL = import.meta.env.VITE_PUBLIC_URL as string;
export const SOCKET_URL = import.meta.env.VITE_SOCKET_URL as string;