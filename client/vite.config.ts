import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  css: {
    preprocessorOptions: {
      styl: {
        imports: ["../src/ui"]
      },
    },
  },
  plugins: [react()]
})
