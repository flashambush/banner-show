import { ProcessPack } from '../server/src/services/ProcessPack';

export const enum BannerType {
    Tgb = 'TGB',
    Html = 'HTML',
    Video = 'VIDEO'
}

export const enum BannerStatus {
    Approved = 'APPROVED',
    Uploaded = 'UPLOADED',
    Error = 'ERROR',
    Deleted = 'DELETED'
}

export type Func<T extends Array<any>, R> = (...args: T) => R;

export type Without<T extends Record<any, any>, E extends keyof T> = {
    [Key in Exclude<keyof T, E>]: T[Key];
}

export type BannerInfo<Date = string> = {
    banner_id: string; // hash(format+platform+MediaPlan.id)
    mediaPlanId: number;
    width: number;
    height: number;
    type: BannerType;
    platform: string;
    /**
     * Статус баннера
     */
    bannerStatus: BannerStatus;
    /**
     * Версия баннера
     */
    version: number;
    /**
     * Дата создания
     */
    createdAt: Date;
    details: string;
}

export type MediaPlanInfo<Date = string> = {
    id: number;
    client: string;
    brand: string;
    campaign: string;
    /**
     * Date of create
     */
    createdAt: Date;
    /**
     * User id
     */
    author: number;
}

export type BannerProcessProgress = ProcessPack.ProgressEvent; //TODO общий баннеро прогресс
export type BannerProcessDone = { id: string }